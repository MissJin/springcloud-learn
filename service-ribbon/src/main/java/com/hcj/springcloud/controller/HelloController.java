package com.hcj.springcloud.controller;

import com.hcj.springcloud.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;


@RestController
@RequestMapping("/")
@lombok.extern.slf4j.Slf4j
public class HelloController {
    @Value("${server.port}")
    private Integer port;
    @Value("${spring.application.name}")
    private String springApplicationName;

    @Autowired
    private HelloService helloService;

    @GetMapping("/hi")
    public String sayHi(String name) {
        log.info("[{}] say hello world, current server:[{}], and port is [{}], and current time is [{}]", name, springApplicationName, port, LocalDateTime.now());
        return helloService.sayHi(name);
    }
}
