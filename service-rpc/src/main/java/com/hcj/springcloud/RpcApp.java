package com.hcj.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RpcApp {

    public static void main(String[] args) {
        SpringApplication.run(RpcApp.class, args);
    }

}
