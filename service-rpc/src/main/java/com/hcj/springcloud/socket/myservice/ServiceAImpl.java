package com.hcj.springcloud.socket.myservice;

public class ServiceAImpl implements ServiceA {
    @Override
    public String sayHi(String name) {
        return "hello " + name;
    }
}
