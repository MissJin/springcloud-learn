package com.hcj.springcloud.socket.test;

import com.hcj.springcloud.socket.RpcClient;
import com.hcj.springcloud.socket.RpcServer;
import com.hcj.springcloud.socket.myservice.ServiceA;
import com.hcj.springcloud.socket.myservice.ServiceAImpl;

import java.net.InetSocketAddress;

public class RunTest {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                RpcServer rpcServer = new RpcServer();
                rpcServer.register(ServiceA.class, ServiceAImpl.class);
                rpcServer.start(10000);
            }
        }).start();

        ServiceA serviceA = RpcClient.getRemoteProxyObj(ServiceA.class, new InetSocketAddress("localhost", 10000));
        String result = serviceA.sayHi("hcj");
        System.out.println("客户端收到的result = " + result);
        // 启动服务 = 2022-08-10T16:28:58.046
        // 当前已注册的服务 = {ServiceA=class com.hcj.springcloud.socket.myservice.ServiceAImpl}
        // 16:28:58.087 [pool-1-thread-1] INFO com.hcj.springcloud.socket.RpcServer$Task - 服务端收到：serviceName:ServiceA
        // 16:28:58.087 [main] INFO com.hcj.springcloud.socket.RpcClient - 客户端发送：serviceName:ServiceA
        // 16:28:58.092 [main] INFO com.hcj.springcloud.socket.RpcClient - 客户端发送：methodName:sayHi
        // 16:28:58.092 [pool-1-thread-1] INFO com.hcj.springcloud.socket.RpcServer$Task - 服务端收到：methodName:sayHi
        // 16:28:58.092 [main] INFO com.hcj.springcloud.socket.RpcClient - 客户端发送：parameterTypes:class java.lang.String
        // 16:28:58.092 [pool-1-thread-1] INFO com.hcj.springcloud.socket.RpcServer$Task - 服务端收到：parameterTypes:class java.lang.String
        // 16:28:58.093 [pool-1-thread-1] INFO com.hcj.springcloud.socket.RpcServer$Task - 服务端收到：args:hcj
        // 16:28:58.093 [main] INFO com.hcj.springcloud.socket.RpcClient - 客户端发送：args:hcj
        // 服务端收计算的result = hello hcj
        // 客户端收到的result = hello hcj
    }
}
