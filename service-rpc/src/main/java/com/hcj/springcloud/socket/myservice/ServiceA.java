package com.hcj.springcloud.socket.myservice;

public interface ServiceA {
    String sayHi(String name);
}
