package com.hcj.springcloud.service.impl;

import com.hcj.springcloud.service.SchedualServiceHi;
import org.springframework.stereotype.Component;

@Component
public class SchedualServiceHiHystric implements SchedualServiceHi {
    @Override
    public String sayHiFromClientOne(String name) {
        return "sorry the server is shutdown , your name is ："+name;
    }
}
