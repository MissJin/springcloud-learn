package com.hcj.springcloud.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 实现callable的方式：实现多线程
 * 可以带返回值
 */
public class MyCallableThread<V> extends OtherClass implements Callable<V> {
    @Override
    public V call() throws Exception {
        System.out.println(Thread.currentThread().getName()+"-->MyCallableThread<V>::call()");
        Integer data = 1;
        return (V) data;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        MyCallableThread<Integer> myCallableThread = new MyCallableThread<>();
        myCallableThread.setName("myCallableThread");
        FutureTask<Integer> oneTask = new FutureTask<>(myCallableThread);
        Thread thread = new Thread(oneTask, myCallableThread.getName());
        thread.start();
        System.out.println("只有任务被执行完成了，才会有返回值："+ oneTask.get());
    }
}
