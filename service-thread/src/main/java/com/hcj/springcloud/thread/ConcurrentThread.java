package com.hcj.springcloud.thread;

import java.util.concurrent.*;

/**
 * java 提供的并发包java.util.concurrent的使用
 *
 * @author huangcj
 * @date 2021-04-26 14:16
 */
public class ConcurrentThread {
    public static ExecutorService newCachedThreadPool() {
        return new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS, new SynchronousQueue<>());
    }

    public static void main(String[] args) {
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("1秒后，执行该语句");
            }
        }, 1, TimeUnit.SECONDS);

        scheduledExecutorService.scheduleAtFixedRate(() -> System.out.println("每隔一秒执行该语句"), 1, 1, TimeUnit.SECONDS);
    }
}
