package com.hcj.springcloud.thread;

/**
 * 继承thread
 */
public class MyExtendsThread extends Thread {

    @Override
    public void run(){
        System.out.println(Thread.currentThread().getName()+ " --> MyExtendsThread::run()");
    }

    public static void main(String[] args) {
        MyExtendsThread t1 = new MyExtendsThread();
        t1.start();
        MyExtendsThread t2 = new MyExtendsThread();
        t2.start();
    }
}
