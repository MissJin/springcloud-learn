package com.hcj.springcloud.thread;

/**
 * 实现runnable来：实现多线程
 */
public class MyRunnableThread extends OtherClass implements Runnable {
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + "--> MyRunnableThread::run()正在被执行");
    }

    public static void main(String[] args) {
        MyRunnableThread myRunnableThread = new MyRunnableThread();
        myRunnableThread.setName("myRunnableThread");

        Thread thread = new Thread(myRunnableThread, myRunnableThread.getName());
        thread.start();
    }
}
