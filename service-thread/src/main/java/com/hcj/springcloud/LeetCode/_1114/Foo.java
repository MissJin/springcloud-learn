package com.hcj.springcloud.LeetCode._1114;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Foo {

    public Foo() {

    }

    public void first(Runnable printFirst) throws InterruptedException {

        // printFirst.run() outputs "first". Do not change or remove this line.
        printFirst.run();
    }

    public void second(Runnable printSecond) throws InterruptedException {

        // printSecond.run() outputs "second". Do not change or remove this line.
        printSecond.run();
    }

    public void third(Runnable printThird) throws InterruptedException {

        // printThird.run() outputs "third". Do not change or remove this line.
        printThird.run();
    }


    public static void main(String[] args) throws InterruptedException {
        Foo foo = new Foo();
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        CountDownLatch countDownLatch = new CountDownLatch(3);
        foo.first(new Runnable() {
            @Override
            public void run() {
                System.out.println("first");
                countDownLatch.countDown();
            }
        });
        foo.second(new Runnable() {
            @Override
            public void run() {
                System.out.println("second");
                countDownLatch.countDown();
            }
        });
        foo.third(new Runnable() {
            @Override
            public void run() {
                System.out.println("third");
                countDownLatch.countDown();
            }
        });
        executorService.shutdown();
    }
}