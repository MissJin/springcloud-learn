package com.hcj.springcloud;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * 文件测试
 *
 * @author huangchangjin
 * @date 2024/7/6 17:28
 */
@Slf4j
public class FilesTest {
    @Test
    public void test() {
        try (Stream<Path> walk = Files.walk(Paths.get("."));) {
            walk.filter(Files::isRegularFile)
                    .filter(x -> !Files.isDirectory(x))
                    .forEach(x -> {
                        log.info("path = {}", x.getFileName());
                    });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
