package com.hcj.springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/")
@lombok.extern.slf4j.Slf4j
public class HelloController {

    @Value("${server.port}")
    private Integer port;
    @Value("${spring.application.name}")
    private String springApplicationName;

    @GetMapping("/hi")
    public String sayHi(String name) {
        return "[" + name + "] say hello world! current server:[" + springApplicationName + "], and port is [" + port + "], and current time is [" + LocalDateTime.now() + "]";
    }
}
