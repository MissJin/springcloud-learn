### webService + springboot2 
- 实现的 跨操作系统的服务器调用（现在集成的是spring）


### 介绍
> 简单来说,webservice就是远程调用技术,也叫XML Web Service WebService是一种可以接收从Internet或者Intranet上的其它系统中传递过来的请求，轻量级的独立的通讯技术。是:通过SOAP在Web上提供的软件服务，使用WSDL文件进行说明，并通过UDDI进行注册。

> XML：(Extensible Markup Language)扩展型可标记语言。面向短期的临时数据处理、面向万维网络，是Soap的基础。
  
> Soap：(Simple Object Access Protocol)简单对象存取协议。是XML Web Service 的通信协议。当用户通过UDDI找到你的WSDL描述文档后，他通过可以SOAP调用你建立的Web服务中的一个或多个操作。SOAP是XML文档形式的调用方法的规范，它可以支持不同的底层接口，像HTTP(S)或者SMTP。
  
> WSDL：(Web Services Description Language) WSDL 文件是一个 XML 文档，用于说明一组 SOAP 消息以及如何交换这些消息。大多数情况下由软件自动生成和使用。
### 搭建
- 1 引入依赖包
```xml
<!--webservice dependencies-->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web-services</artifactId>
</dependency>
<dependency>
    <groupId>wsdl4j</groupId>
    <artifactId>wsdl4j</artifactId>
</dependency>

<dependency>
    <groupId>org.apache.cxf</groupId>
    <artifactId>cxf-rt-frontend-jaxws</artifactId>
    <version>RELEASE</version>
</dependency>
<dependency>
    <groupId>org.apache.cxf</groupId>
    <artifactId>cxf-rt-transports-http</artifactId>
    <version>RELEASE</version>
</dependency>
```
- 2 编写service【MyWebService】
```java
package com.hcj.springcloud.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;


@WebService(name = "MyWebService", targetNamespace = "http://service.springcloud.hcj.com/")
public interface MyWebService {
    @WebMethod
    String sayHello(@WebParam(name = "name") String name);

}
```
- 3 编写实现类【MyWebServiceImpl】
```java
package com.hcj.springcloud.service.impl;

import com.hcj.springcloud.service.MyWebService;
import org.springframework.stereotype.Service;

import javax.jws.WebService;

@Service
@WebService(serviceName = "myWebService", // 与接口中指定的name一致
        targetNamespace = "http://impl.service.springcloud.hcj.com", // 与接口中的命名空间一致,一般是接口的包名倒序
        endpointInterface = "com.hcj.springcloud.service.MyWebService" // 接口地址
)
public class MyWebServiceImpl implements MyWebService {

    @Override
    public String sayHello(String name) {
        return name;
    }
}
```
- 4 编写整合spring的cxf配置文件
```java
package com.hcj.springcloud.config;

import com.hcj.springcloud.service.MyWebService;
import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import javax.xml.ws.Endpoint;

@Configuration
public class CxfConfig {

    @Resource
    private MyWebService myWebService;

    @Bean(name = "cxfServlet")
    public ServletRegistrationBean dispatcherServlet() {
        return new ServletRegistrationBean(new CXFServlet(), "/webService/*");
    }

    @Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        return new SpringBus();
    }

    @Bean(name = "WebServiceDemoEndpoint")
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(springBus(), myWebService);
        endpoint.publish("/api");
        return endpoint;
    }
}

```

### cxfClient调用webservice
```java
package com.hcj.springcloud.client;

import com.hcj.springcloud.genJavaCodeFromWsdl.MyWebService;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.junit.Test;

/**
 * webService客戶端的调用方法
 * 1.代理类工厂的方式,需要拿到对方的接口地址
 * 2：动态调用
 */
public class CxfClient {

    /**
     * 1.代理类工厂的方式,需要拿到对方的接口地址
     */
    @Test
    public void a_test() {
        try {
            // 接口地址
            String address = "http://127.0.0.1:8080/webService/api?wsdl";
            // 代理工厂
            JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
            // 设置代理地址
            jaxWsProxyFactoryBean.setAddress(address);
            // 设置接口类型
            jaxWsProxyFactoryBean.setServiceClass(MyWebService.class);
            // 创建一个代理接口实现
            MyWebService us = (MyWebService) jaxWsProxyFactoryBean.create();
            // 数据准备
            String params = "hello everyone!";
            // 调用代理接口的方法调用并返回结果
            String result = us.sayHello(params);
            System.out.println("返回结果:" + result); // 返回数据:hello everyone!
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 2：动态调用
     */
    @Test
    public void b_test() {
        // 创建动态客户端
        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        Client client = dcf.createClient("http://127.0.0.1:8080/webService/api?wsdl");
        // 需要密码的情况需要加上用户名和密码
        // client.getOutInterceptors().add(new ClientLoginInterceptor(USER_NAME, PASS_WORD));
        Object[] objects = new Object[0];
        try {
            // invoke("方法名",参数1,参数2,参数3....);
            objects = client.invoke("sayHello", "hello everyone!"); // 返回数据:hello everyone!
            System.out.println("返回数据:" + objects[0]);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }
}

```

### 相关截图
![webService启动后.png](screenshot/webService启动后.png)
***
![运行webService服务器后，生成代码，方便客户端调用.png](screenshot/运行webService服务器后，生成代码，方便客户端调用.png)
***
![生成的代码.png](screenshot/生成的代码.png)
***
![模拟客户端测试webService的接口.png](screenshot/模拟客户端测试webService的接口.png)
