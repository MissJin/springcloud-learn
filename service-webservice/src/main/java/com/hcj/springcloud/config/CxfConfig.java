package com.hcj.springcloud.config;

import com.hcj.springcloud.interceptor.AuthInterceptor;
import com.hcj.springcloud.interceptor.ClientLoginInterceptor;
import com.hcj.springcloud.service.MyWebService;
import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.interceptor.Interceptor;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import javax.xml.ws.Endpoint;
import java.util.List;

@Configuration
public class CxfConfig {

    @Resource
    private MyWebService myWebService;

    @Bean(name = "cxfServlet")
    public ServletRegistrationBean dispatcherServlet() {
        return new ServletRegistrationBean(new CXFServlet(), "/webService/*");
    }

    @Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        return new SpringBus();
    }

    @Bean(name = "WebServiceDemoEndpoint")
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(springBus(), myWebService);
        endpoint.publish("/api");
        List<Interceptor<? extends Message>> inInterceptors = endpoint.getInInterceptors();
        inInterceptors.add(new ClientLoginInterceptor());
        inInterceptors.add(new AuthInterceptor());
        return endpoint;
    }
}
