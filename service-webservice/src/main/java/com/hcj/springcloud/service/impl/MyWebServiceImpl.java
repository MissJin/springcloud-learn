package com.hcj.springcloud.service.impl;

import com.hcj.springcloud.service.MyWebService;
import org.springframework.stereotype.Service;

import javax.jws.WebService;

@Service
@WebService(serviceName = "myWebService", // 与接口中指定的name一致
        targetNamespace = "http://service.springcloud.hcj.com", // 与接口中的命名空间一致,一般是接口的包名倒序
        endpointInterface = "com.hcj.springcloud.service.MyWebService" // 接口地址
)
public class MyWebServiceImpl implements MyWebService {

    @Override
    public String sayHello(String name) {
        return name;
    }
}
