
package com.hcj.springcloud.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;


@WebService(name = "MyWebService", targetNamespace = "http://service.springcloud.hcj.com")
public interface MyWebService {
    @WebMethod
    String sayHello(@WebParam(name = "name") String name);

}
