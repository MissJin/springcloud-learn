package com.hcj.springcloud;

import com.hcj.springcloud.genJavaCodeFromWsdl.MyWebService;
import com.hcj.springcloud.genJavaCodeFromWsdl.MyWebService_Service;
import com.hcj.springcloud.interceptor.ClientLoginInterceptor;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.junit.Test;

/**
 * 适用于： 已知 wsdl后（即，webservice的服务端endpoint），
 * 用 idea自带的"generate java code from wsdl",生成java代码，
 * 再调用具体的方法
 */
public class JavaCodeFromWsdlTest {

    private static final String USER_NAME = "admin";
    private static final String PASS_WORD = "123456";

    public static final int CXF_CLIENT_CONNECT_TIMEOUT = 30 * 1000;
    public static final int CXF_CLIENT_RECEIVE_TIMEOUT = 30 * 1000;

    @Test
    public void testWebService() {
        MyWebService_Service service_service = new MyWebService_Service();
        MyWebService myWebServiceImplPort = service_service.getMyWebServiceImplPort();

        // 访问时===》 开启验证码的校验---》start
        Client proxy = ClientProxy.getClient(myWebServiceImplPort);
        proxy.getOutInterceptors().add(new ClientLoginInterceptor(USER_NAME, PASS_WORD));
        // 通过本地客户端设置 网络策略配置
        HTTPConduit conduit = (HTTPConduit) proxy.getConduit();
        // 用于配置客户端HTTP端口的属性
        HTTPClientPolicy policy = new HTTPClientPolicy();
        // 超时控制 单位 : 毫秒
        policy.setConnectionTimeout(CXF_CLIENT_CONNECT_TIMEOUT);
        policy.setReceiveTimeout(CXF_CLIENT_RECEIVE_TIMEOUT);
        conduit.setClient(policy);
        // 访问时===》 开启验证码的校验---》end


        String hello = myWebServiceImplPort.sayHello("测试genJavaCodeForWsdl中的代码");
        System.out.println("webService的回应===>" + hello);
    }
}
