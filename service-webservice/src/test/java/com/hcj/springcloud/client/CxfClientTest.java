package com.hcj.springcloud.client;

import com.hcj.springcloud.genJavaCodeFromWsdl.MyWebService;
import com.hcj.springcloud.interceptor.ClientLoginInterceptor;
import org.apache.cxf.configuration.security.AuthorizationPolicy;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.junit.Test;

/**
 * webService客戶端的调用方法
 * 1.代理类工厂的方式,需要拿到对方的接口地址
 * 2：动态调用
 */
public class CxfClientTest {

    private static final String USER_NAME = "admin";
    private static final String PASS_WORD = "123456";

    public static final int CXF_CLIENT_CONNECT_TIMEOUT = 30 * 1000;
    public static final int CXF_CLIENT_RECEIVE_TIMEOUT = 30 * 1000;

    /**
     * 1.代理类工厂的方式,需要拿到对方的接口地址
     */
    @Test
    public void a_test() {
        try {
            // 接口地址
            String address = "http://127.0.0.1:8080/webService/api?wsdl";
            // 代理工厂
            JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
            // 设置代理地址
            jaxWsProxyFactoryBean.setAddress(address);
            // 设置接口类型
            jaxWsProxyFactoryBean.setServiceClass(MyWebService.class);
            // 创建一个代理接口实现
            MyWebService webService = (MyWebService) jaxWsProxyFactoryBean.create();


            // ############################# 设置访问的用户名和密码 ######start#############
            // 设置接口 连接超时和请求超时
            // 通过代理对象获取本地客户端
            Client proxy = ClientProxy.getClient(webService);
            proxy.getOutInterceptors().add(new ClientLoginInterceptor(USER_NAME, PASS_WORD));
            // 通过本地客户端设置 网络策略配置
            HTTPConduit conduit = (HTTPConduit) proxy.getConduit();
            // 用于配置客户端HTTP端口的属性
            HTTPClientPolicy policy = new HTTPClientPolicy();
            // 超时控制 单位 : 毫秒
            policy.setConnectionTimeout(CXF_CLIENT_CONNECT_TIMEOUT);
            policy.setReceiveTimeout(CXF_CLIENT_RECEIVE_TIMEOUT);
            conduit.setClient(policy);
            // ############################# 设置访问的用户名和密码 ######end#############

            // 数据准备
            String params = "hello everyone!";
            // 调用代理接口的方法调用并返回结果
            String result = webService.sayHello(params);
            System.out.println("返回结果:" + result); // 返回数据:hello everyone!
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 2：动态调用
     */
    @Test
    public void b_test() {
        // 创建动态客户端
        JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory.newInstance();
        Client client = dcf.createClient("http://127.0.0.1:8080/webService/api?wsdl");
        // 需要密码的情况需要加上用户名和密码
        client.getOutInterceptors().add(new ClientLoginInterceptor(USER_NAME, PASS_WORD));
        Object[] objects = new Object[0];
        try {
            // invoke("方法名",参数1,参数2,参数3....);
            objects = client.invoke("sayHello", "hello everyone!"); // 返回数据:hello everyone!
            System.out.println("返回数据:" + objects[0]);
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }
}
