package com.hcj.springcloud.huawei;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * huawei 机考
 *
 * @author huangcj
 * @date 2021-04-14 10:15
 */

@Slf4j
public class HuaweiTest {


    public static void testA() {
        Scanner scan = new Scanner(System.in);
        while (scan.hasNext()) {
            String str = scan.nextLine();
            String[] ss = str.split(" ");
            System.out.println(ss[ss.length - 1].length());
        }
    }

    public static void testB() throws IOException {
        InputStream in = System.in;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
        char[] chars = bufferedReader.readLine().toLowerCase().toCharArray();
        char c = (char) bufferedReader.read();
        int count = 0;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == c) {
                count++;
            }
        }
        System.out.println(count);
    }

    public static void testC() throws IOException {
        InputStream in = System.in;
        BufferedReader bfr = new BufferedReader(new InputStreamReader(in));
        String str;
        List<Integer> nums = new ArrayList<>(1001);
        while ((str = bfr.readLine()) != null && !str.equals("")) {
            int num = Integer.valueOf(str);
            for (int i = 0; i < num; i++) {
                nums.add(Integer.valueOf(bfr.readLine()));
            }
        }
        List<Integer> sortedList = nums.stream().distinct().sorted().collect(Collectors.toList());
        sortedList.stream().forEachOrdered(x -> System.out.println(x));
    }

    public static void main(String[] args) throws IOException {
        //testA();
        //testB();
        testC();
    }
}
