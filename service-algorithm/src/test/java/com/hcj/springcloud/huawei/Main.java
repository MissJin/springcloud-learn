package com.hcj.springcloud.huawei;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws Exception {
        //HJ31();
        //HJ23();
    }

    /**
     * 单词倒叙排序
     */
    public static void HJ31() throws Exception {
        InputStream in = System.in;
        int available = in.available();
        char[] arr = new char[available];
        int off_word = 0;
        int off_end = arr.length;
        char c;
        for (int i = 0; i < available; i++) {
            c = (char) in.read();
            if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
                arr[off_word++] = c;
            } else if (off_word > 0) {
                System.arraycopy(arr, 0, arr, off_end - off_word, off_word);
                off_end -= off_word + 1;
                off_word = 0;
                arr[off_end] = ' ';
            }
        }
        System.out.println(String.valueOf(arr, off_end + 1, arr.length - off_end - 1));
    }

    /**
     * 删除字符串中出现最少的字符，并输出
     */
    public static void HJ23() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str;
        while ((str = br.readLine()) != null) {
            int[] num = new int[26];
            char[] chs = str.toCharArray();
            for (int i = 0; i < chs.length; i++) {
                int n = chs[i] - 'a';
                num[n] = num[n] + 1;
            }
            int min = Integer.MAX_VALUE;
            for (int i = 0; i < 26; i++) {
                if (num[i] != 0 && num[i] < min) {
                    min = num[i];
                }
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < chs.length; i++) {
                int n = chs[i] - 'a';
                if (num[n] != min) {
                    sb.append(chs[i]);
                }
            }
            System.out.println(sb.toString());
        }
    }
}
