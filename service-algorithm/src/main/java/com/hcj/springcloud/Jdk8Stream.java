package com.hcj.springcloud;

import junit.framework.TestCase;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
public class Jdk8Stream extends TestCase {
    /**
     * 平均值
     */
    public void test_average_collectors() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4);
        Double average = list.stream().collect(Collectors.averagingDouble(x -> x + 1));
        log.info("average: {}", average);
    }

    /**
     * 展平数据
     */
    public void test_flatmap() {
        List<Integer> listA = Arrays.asList(1, 2, 3, 4);
        List<Integer> listB = Arrays.asList(5, 6);
        List<List<Integer>> result = new ArrayList<>();
        result.add(listA);
        result.add(listB);
        // 对result列表中的数据，展平
        List<Integer> flatmap = result.stream().flatMap(x -> x.stream()).collect(Collectors.toList());
        // flatmap: [1, 2, 3, 4, 5, 6]
        log.info("flatmap: {}", flatmap);
    }

    /**
     * 快速mock固定数据
     */
    public void test_mock_data_fast() {
        List<String> mockData = IntStream.range(0, 3).boxed().parallel().map(x -> "prefix_" + x + "_suffix").collect(Collectors.toList());
        // mockData:[prefix_0_suffix, prefix_1_suffix, prefix_2_suffix]
        log.info("mockData:{}", mockData);
    }
}
