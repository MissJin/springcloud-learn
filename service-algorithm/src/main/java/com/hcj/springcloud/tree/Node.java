package com.hcj.springcloud.tree;

import com.alibaba.fastjson.JSON;
import lombok.Data;

/**
 * 节点
 */
@Data
public class Node<T> {
    private T data;
    private Node left;
    private Node right;

    public Node(T data) {
        this.data = data;
    }

    public void display(){
        System.out.println(data);
    }

    public void toJSON(){
        System.out.println(JSON.toJSONString(this));
    }
}
