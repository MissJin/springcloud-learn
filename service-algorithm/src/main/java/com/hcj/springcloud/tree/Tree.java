package com.hcj.springcloud.tree;

public interface Tree<T> {
    /** 查找*/
    Node<T> find(T data);

    /** 插入*/
    Boolean insert(T data);

    /** 删除*/
    Boolean delete(T data);

    /** 查找最大值*/
    Node<T> findMax();

    /** 查找最小值*/
    Node<T> findMin();

    /** 中序遍历: 值是从小到大顺序排列 */
    void infixOrder(Node<T> root);

    /** 前序遍历*/
    void preOrder(Node<T> root);

    /** 后序遍历*/
    void postOrder(Node<T> root);


}
