package com.hcj.springcloud.springbean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author huangcj
 * @date 2021-04-27 15:25
 */
@Service
public class B {
    @Autowired
    private A a;

    public B() {
        System.out.println("a = " + a);
    }
}
