package com.hcj.springcloud.springbean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author huangcj
 * @date 2021-04-27 15:24
 */
@Service
public class A {
    @Autowired
    private B b;
}
