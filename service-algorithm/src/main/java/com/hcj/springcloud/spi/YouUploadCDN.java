package com.hcj.springcloud.spi;

/**
 * @author huangcj
 * @date 2021-04-23 11:25
 */
public class YouUploadCDN implements UploadCDN {
    @Override
    public void upload() {
        System.out.println("YouUploadCDN = " + true);
    }
}
