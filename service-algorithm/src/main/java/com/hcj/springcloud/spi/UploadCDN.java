package com.hcj.springcloud.spi;

/**
 * 上传接口
 * @author huangcj
 * @date 2021-04-23 11:24
 */
public interface UploadCDN {
    void upload();
}
