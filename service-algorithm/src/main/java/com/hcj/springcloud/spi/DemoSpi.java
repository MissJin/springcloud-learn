package com.hcj.springcloud.spi;

import java.util.ServiceLoader;

/**
 * SPI全称Service Provider Interface，是Java提供的一套用来被第三方实现或者扩展的接口，它可以用来启用框架扩展和替换组件。 SPI的作用就是为这些被扩展的API寻找服务实现。
 * 参考 https://www.cnblogs.com/jy107600/p/11464985.html
 * 实现步骤：
 * 1. 创建一个接口MyI
 * 2. 定义好接口的不同实现类A,B,C
 * 3. 在 resources/META-INF/services 目录下添加 一个以接口I的类文件全限定名的文件，里面内容放置A,B,C的接口实现类名
 * 4. 利用jdk的工具类java.util.ServiceLoader.load(MyI.class)，调用
 *
 * @author huangcj
 * @date 2021-04-23 11:29
 */
public class DemoSpi {
    /**
     * API （Application Programming Interface）在大多数情况下，都是实现方制定接口并完成对接口的实现，调用方仅仅依赖接口调用，且无权选择不同实现。 从使用人员上来说，API 直接被应用开发人员使用。
     * <p>
     * SPI （Service Provider Interface）是调用方来制定接口规范，提供给外部来实现，调用方在调用时则选择自己需要的外部实现。  从使用人员上来说，SPI 被框架扩展人员使用。
     */
    public static void main(String[] args) {
        ServiceLoader<UploadCDN> loads = ServiceLoader.load(UploadCDN.class);
        for (UploadCDN load : loads) {
            load.upload();
            System.out.println("当前的接口实现类实例为： = " + load);
            // 当前的接口实现类实例为： = com.hcj.springcloud.spi.MyUploadCDN@46fbb2c1
            // 当前的接口实现类实例为： = com.hcj.springcloud.spi.YouUploadCDN@1698c449
        }
    }
}

