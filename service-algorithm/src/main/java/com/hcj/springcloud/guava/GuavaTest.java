package com.hcj.springcloud.guava;

import com.google.common.base.CharMatcher;
import com.google.common.base.MoreObjects;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.io.Files;
import com.google.common.io.Resources;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;

/**
 * @author huangcj
 * @date 2020-09-23 17:55
 */
public class GuavaTest {

    /**
     * 对于InputStream读取中文乱码，下面这段话给出了很好的解释，以及后续编码上的扩展。
     * BufferedInputStream和BufferedOutputStream是过滤流，需要使用已存在的节点来构造。
     * 即必须先有InputStream或OutputStream，相对直接读写，这两个流提供带缓存的读写，提高了系统读写效率性能。
     * BufferedInputStream读取的是字节byte，因为一个汉字占两个字节，而当中英文混合的时候，有的字符占一个字节，有的字符占两个字节。
     * 所以如果直接读字节，而数据比较长，没有一次读完的时候，很可能刚好读到一个汉字的前一个字节，这样，这个中文就成了乱码，后面的数据因为没有字节对齐，也都成了乱码。
     * 所以我们需要用BufferedReader来读取，它读到的是字符，所以不会读到半个字符的情况，不会出现乱码。
     * @author huangcj
     * @date 2020-09-24 10:57
     */
    public static void fileReader() throws IOException {
        // 文件读写操作
        File file = new File("readme.md");
        List<String> strs = Files.readLines(file, Charset.defaultCharset());
        System.out.println("====》guava 工具类直接读取一个文件 strs = " + strs);
        URL resource = Resources.getResource("static/test.txt");
        String path = resource.getPath();
        System.out.println("====》path = " + path);
        InputStream in1 = resource.openStream();
        InputStream in2 = resource.openStream();
        InputStream in3 = resource.openStream();
        BufferedInputStream bufferedInputStream1 = new BufferedInputStream(in1);
        BufferedInputStream bufferedInputStream2 = new BufferedInputStream(in2);
        BufferedInputStream bufferedInputStream3 = new BufferedInputStream(in3);
        BufferedReader bufferedReader1 = new BufferedReader(new InputStreamReader(bufferedInputStream1));
        BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(bufferedInputStream2));
        BufferedReader bufferedReader3 = new BufferedReader(new InputStreamReader(bufferedInputStream3));
        StringBuffer sb1 = new StringBuffer();
        StringBuffer sb2 = new StringBuffer();
        StringBuffer sb3 = new StringBuffer();

        // 方法1： 一个字符一个字符读取的形式，能解决中英文乱码的情况
        while (bufferedReader1.ready()){
            char oneChar = (char) bufferedReader1.read();
            System.err.println("oneChar = " + oneChar);
            sb1.append(oneChar);
        }
        System.out.println("\n一个字符一个字符读取的形式，能解决中英文乱码\nsb1 ===》 " + sb1);

        // 方法2：10个字符一起读取的形式，能解决中英文乱码和效率的问题
        char[] cbuf = new char[10];
        int len;
        while (bufferedReader2.ready() && (len = bufferedReader2.read(cbuf)) != -1){
            String tenChar = new String(cbuf, 0, len);
            System.err.println("read 10 char = " + tenChar);
            sb2.append(tenChar);
        }
        System.out.println("10个字符一起读取的形式，能解决英文乱码和效率的问题\nsb2 ===》 " + sb2);


        // 方法3：1行1行的读取，可解决中英文乱码
        while (bufferedReader3.ready()){
            String oneLine = bufferedReader3.readLine();
            System.err.println("read one line = " + oneLine);
            sb3.append(oneLine);
        }
        System.out.println("1行1行的读取，可解决中英文乱码 \nsb3 ===》 " + sb3);

        in1.close();
        in2.close();
        in3.close();
        bufferedInputStream1.close();
        bufferedInputStream2.close();
        bufferedInputStream3.close();
        bufferedReader1.close();
        bufferedReader2.close();
        bufferedReader3.close();
    }
    public static void main(String[] args) throws IOException {
        ArrayListMultimap<String, Integer> mapM = ArrayListMultimap.create();
        mapM.put("list1", 1);
        mapM.put("list1", 2);
        mapM.put("list1", 2);
        mapM.put("list2", 1);
        mapM.put("list2", 2);
        mapM.put("list2", 2);
        List<Integer> list1 = mapM.get("list1");
        List<Integer> list2 = mapM.get("list2");


        // 字符串中 空格/换行 替换
        String s = CharMatcher.whitespace().collapseFrom("you  \r\n  are smart  !", ' ');
        System.out.println("s = " + s);


        // toString字符串的使用
        String str = MoreObjects.toStringHelper(mapM).toString();
        System.out.println("str = " + str);



        // 代码执行耗时
        Stopwatch started = Stopwatch.createStarted();
        try {
            Thread.sleep(100L);
        }catch (Exception e){
            System.out.println("e.getMessage() = " + e.getMessage());
        }
        System.out.println("耗时 = " + started.stop());

        // 测试读文件
        fileReader();


    }
}
