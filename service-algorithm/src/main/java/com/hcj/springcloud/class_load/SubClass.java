package com.hcj.springcloud.class_load;

public class SubClass extends SuperClass{
    static {
        System.out.println("sub class static init");
    }
    public static int B = value;
}
