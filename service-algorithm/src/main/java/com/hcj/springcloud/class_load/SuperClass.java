package com.hcj.springcloud.class_load;

public class SuperClass {
    /** 会有现后顺序 看谁的代码在前面 */
    public static int value = 3;

    static {
        System.out.println("super class static init");
        value = 1;
    }


}



