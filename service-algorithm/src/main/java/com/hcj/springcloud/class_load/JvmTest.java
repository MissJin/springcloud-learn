package com.hcj.springcloud.class_load;

/**
 * jvm类属性加载顺序问题
 */
public class JvmTest {
    public static JvmTest jt = new JvmTest();
    public static int a;
    public static int b = 0;

    static {
        a++;
        b++;
    }

    public JvmTest() {
        a++;
        b++;
    }

    public static void main(String[] args) {
        /**
         * 准备阶段：为 jt、a、b 分配内存并赋初始值 jt=null、a=0、b=0
         * 解析阶段：将 jt 指向内存中的地址
         * 初始化：jt 代码位置在最前面，这时候 a=1、b=1
         *          a 没有默认值，不执行，a还是1，b 有默认值，b赋值为0
         *          静态块过后，a=2、b=1
         */
        System.out.println(a);
        System.out.println(b);
    }
}
