package com.hcj.springcloud.class_load;

public class NotInitialization {
    public static void main(String[] args) {
        /**
         * 1.通过子类引用父类的静态字段，不会导致子类初始化。
         * 对于静态字段，只有直接定义这个字段的类才会被初始化。
         */
        System.out.println(SubClass.value);
        // super class static init
        // 3

        /**
         * 2.通过数组定义来引用类，不会触发此类的初始化
         */
        SuperClass[] superClasses = new SuperClass[]{};

        /**
         * 3.常量在编译阶段会存入调用类的常量池中，本质上并没有直接引用到定义常量的类，因此不会触发定义常量的类的初始化。
         * 编译通过之后，常量存储到 NotInitialization 类的常量池中，
         * NotInitialization 的 Class 文件中并没有 ConstClass 类的符号引用入口，
         * 这两个类在编译成 Class 之后就没有任何联系了。
         */
        System.out.println(ConstClass.value);


        /**
         * 1.1虚拟机会保证在子类的 <clinit>() 方法执行之前，父类的 <clinit>() 方法已经执行完毕。
         *
         * 由于父类的 <clinit>() 方法先执行，意味着父类中定义的静态语句块要优先于子类的变量赋值操作
         */
        System.out.println(SubClass.B);
        // super class static init
        // sub class static init
        // 3
    }
}
