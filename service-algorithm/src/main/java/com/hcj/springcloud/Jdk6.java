package com.hcj.springcloud;

import java.io.Console;
import java.io.PrintWriter;

/**
 * jdk6 新特性
 */
public class Jdk6 {
    public static void main(String[] args) {
        Console cons = System.console();
        if (cons != null) {

            PrintWriter printWriter = cons.writer();
            printWriter.write("input:");
            cons.flush();
            String str1 = cons.readLine();
            cons.format("%s", str1);
        }
    }
}
