package com.hcj.springcloud.controller;

import com.hcj.springcloud.dto.Dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

@RestController
public class TestController {

    /**
     * 发送请求：http://localhost:8080/testSpringCahe?key=hello
     * 结果将被缓存下来：
     */
    @Cacheable(value = "CACHE_NAME_10_SECOND", key = "#key", condition = "#key == 'hello'")
    @GetMapping("/testSpringCahe")
    public Object testSpringCache(String key) {
        return getSpringCache(key);
    }

    /**
     * <blockquote>
     * <p>（1）@Cacheable，首先在缓存中查询，没有的话，进方法执行，执行完成后返回值放缓存；</p>
     * <p>（2）@CachePut，不进行查询判断，直接新增或者更新缓存内容，类似add或update；</p>
     * <p>（3）@CacheEvict，直接清除，类似delete。</p>
     * <p>（4）@Caching，1,2,3个组合</p>
     * </blockquote>
     * <pre>
     * value	    缓存的名称，在 spring 配置文件中定义，必须指定至少一个	    例如: @Cacheable(value=”laowang_cache1”) @Cacheable(value={””laowang_cache1,”laowang_cache2”}
     * key	    缓存的 key，可以为空，如果指定要按照 SpEL 表达式编写，如果不指定，则缺省按照方法的所有参数进行组合	@Cacheable(value=”laowang_cache1”,key=”#userName”)
     * condition	缓存的条件，可以为空，使用 SpEL 编写，返回 true 或者 false，只有为 true 才进行缓存	@Cacheable(value=”laowang_cache1”, condition=”#userName.length()>11”)
     * </pre>
     */
    public Object getSpringCache(String key) {
        System.out.println("未从缓存中获取:" + LocalDateTime.now());
        String object = key + new Random().nextInt(10);
        return object;
    }

    @PostMapping("/test")
    public Object test(@RequestBody Dto dto) {
        for(String s: dto.getIds()){

        }
        return dto;
    }
}
