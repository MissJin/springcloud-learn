package com.hcj.springcloud.dto;

import lombok.Data;

import java.util.List;

@Data
public class Dto {
    private List<String> ids;
}
