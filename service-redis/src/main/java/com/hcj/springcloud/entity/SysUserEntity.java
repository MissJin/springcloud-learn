package com.hcj.springcloud.entity;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class SysUserEntity {
    private String id;
    private String name;
    private String email;
}
