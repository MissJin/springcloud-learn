package com.hcj.springcloud;

import com.google.gson.Gson;
import com.hcj.springcloud.entity.SysUserEntity;
import com.hcj.springcloud.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisSetCommands;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.*;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {
    @Resource
    private RedisUtils redisUtils;
    private final static Gson gson = new Gson();

    /**
     * 流水线：
     * 用途： 用于快速批量存/取元素，降低redis单线程在网络传输中受到的影响
     */
    @Test
    public void test_pipelined() {
        long start = System.currentTimeMillis();
        List<Object> objectList = redisUtils.executePipelined(new RedisCallback<Object>() {
            @Override
            public Long doInRedis(RedisConnection connection) throws DataAccessException {
                connection.openPipeline();
                RedisSetCommands redisSetCommands = connection.setCommands();
                for (int i = 0; i < 10000; i++) {
                    String key = "test_pipeline:" + i;
                    //connection.zCount(key.getBytes(), 0, Integer.MAX_VALUE);
                    //connection.set(key.getBytes(), String.valueOf(i).getBytes());
                    redisSetCommands.sAdd(key.getBytes(), String.valueOf(i).getBytes());
                    //redisUtils.set(key, i);
                }

                connection.closePipeline();
                return null;
            }
        });
        long end = System.currentTimeMillis();
        log.info("耗时：{} 毫秒", (end - start));// 耗时：98 毫秒
    }

    /**
     * @return void
     * @Description 耗时比较长
     * @Author hcj
     * @CreateTime 2019-03-29 15:00
     * @Param []
     **/
    @Test
    public void test_not_use_pipelined() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10000; i++) {
            String key = "test_no_pipeline:" + i;
            redisUtils.set(key, i);
        }
        long end = System.currentTimeMillis();
        log.info("耗时：{} 毫秒", (end - start));//耗时：465463 毫秒
    }


    @Test
    public void test_str_inc() {
        Boolean test_inc = redisUtils.existKey("test_inc");
        log.info("redisUtils.existKey(\"test_inc\") =>>>>> {}", test_inc);
        redisUtils.inc("test_inc", 120L);
        log.info("直接从redis获取到 test_inc is {}", redisUtils.get("test_inc"));
        redisUtils.inc("test_inc", 120L);
        log.info("直接从redis获取到 test_inc is {}", redisUtils.get("test_inc"));
        redisUtils.inc("test_inc", 120L);
        log.info("直接从redis获取到 test_inc is {}", redisUtils.get("test_inc"));
    }


    @Test
    public void test_str() {
        SysUserEntity user = new SysUserEntity();
        user.setEmail("qqq@qq.com");
        redisUtils.set("user", user);
        log.info("直接从redis获取到 user is {}", redisUtils.get("user"));
        SysUserEntity user_from_redis = gson.fromJson(redisUtils.get("user"), SysUserEntity.class);
        log.info("json转对象后, user_from_redis is {}", user_from_redis);
        String user_str = ToStringBuilder.reflectionToString(redisUtils.get("user", SysUserEntity.class));
        log.info("user to str is {}", user_str);
    }

    /**
     * @return void
     * @Description 依赖序列化来转化：
     * 现在是字符串序列化，所以存储的时候value要转换程json;
     * 读取的时候，value要逆转化为相应的对象
     * @Author hcj
     * @CreateTime 2019-02-12 09:33
     * @Param []
     **/
    @Test
    public void test_hash() {
        SysUserEntity user1 = new SysUserEntity();
        user1.setEmail("1@qq.com");
        SysUserEntity user2 = new SysUserEntity();
        user2.setEmail("2@qq.com");
        SysUserEntity user3 = new SysUserEntity();
        user3.setEmail("3@qq.com");
        redisUtils.hset("test_hash", "user1", gson.toJson(user1));
        redisUtils.hset("test_hash", "user2", gson.toJson(user2));
        redisUtils.hset("test_hash", "user3", gson.toJson(user3));

        String user1_from_redis = (String) redisUtils.hget("test_hash", "user1");
        SysUserEntity sysUserEntity = gson.fromJson(user1_from_redis, SysUserEntity.class);
        log.info("user1_from_redis is {}", user1_from_redis);
        log.info("user1_from_redis gson为Object后 {}", sysUserEntity.toString());

		/*
		// 需要开启json序列化
		SysUserEntity user3_from_redis = (SysUserEntity)redisUtils.hget("test_hash", "user3");
		log.info("user3_from_redis 后 {}", user3_from_redis.getEmail());

		Map map = new HashMap(2);
		map.put("a","你");
		map.put("b","好");
		redisUtils.hset("test_hash", "map", map);
		Map map_from_redis = (Map)redisUtils.hget("test_hash", "map");
		log.info("map_from_redis is {}", map_from_redis);
		log.info("map_from_redis map为Object后 {}", map.get("a"));
		*/
        Map map = new HashMap(2);
        map.put("a", "你");
        map.put("b", "好");
        redisUtils.hset("test_hash", "map", gson.toJson(map));
        Object map_from_redis = redisUtils.hget("test_hash", "map");
        Map map_from_gson = gson.fromJson((String) map_from_redis, Map.class);
        log.info("map_from_redis is {}", map_from_redis);
        log.info("map_from_redis map为Object后 {}", map_from_gson.get("a"));


        Map user1_to_map = gson.fromJson(user1_from_redis, Map.class);
        log.info("测试json转map {}", user1_to_map.get("email"));

    }

    /**
     * @return void
     * @Description
     * @Author hcj
     * @CreateTime 2019-02-12 10:13
     * @Param []
     **/
    @Test
    public void test_list() {
        ListOperations<String, Object> listOperations = redisUtils.getListOperations();
        listOperations.leftPush("test_list", "l1");
        listOperations.leftPush("test_list", "l2");
        listOperations.leftPush("test_list", "l3");
        listOperations.rightPush("test_list", "r4");
        listOperations.rightPush("test_list", "r5");
        listOperations.rightPush("test_list", "r6");
        Object test_list = listOperations.index("test_list", 0);
        log.info("test_list is {}", test_list);
        List<Object> test_list_range = listOperations.range("test_list", 0, -1);
        log.info("test_list_range is {}", Arrays.asList(test_list_range).toString());//test_list_range is [[l3, l2, l1, r4, r5, r6]]
        test_list_range.stream().forEach(x -> {
            log.info("{}", x);
        });//l3, l2, l1, r4, r5, r6
    }

    @Test
    public void test_set() {
        SetOperations<String, Object> setOperations = redisUtils.getSetOperations();
        setOperations.add("test_set", "a");
        setOperations.add("test_set", "b");
        setOperations.add("test_set", "c");
        setOperations.add("test_set", "c");
        Set<Object> test_set = setOperations.members("test_set");
        test_set.stream().forEach(x -> log.info("test_set 中members {}", x));// a b c
        // mock 另外一个key
        setOperations.add("test_set_o", "a");
        setOperations.add("test_set_o", "b");
        setOperations.add("test_set_o", "d");

        // 取交集
        Set<Object> intersect = setOperations.intersect("test_set", "test_set_o");
        intersect.stream().forEach(x -> log.info("取交集 {}", x));// a b
        // 取全集
        Set<Object> union = setOperations.union("test_set", "test_set_o");
        union.stream().forEach(x -> log.info("取全集 {}", x));// d a b c
    }

    @Test
    public void test_zset() {
        ZSetOperations<String, Object> zSetOperations = redisUtils.getZSetOperations();
        redisUtils.getRedisTemplate().delete("test_zset");
        zSetOperations.add("test_zset", "a", 0);
        zSetOperations.add("test_zset", "b", 1);
        zSetOperations.add("test_zset", "c", 2);
        zSetOperations.add("test_zset", "d", 3);
        zSetOperations.add("test_zset", "f", 3);
        Set<ZSetOperations.TypedTuple<Object>> test_zset = zSetOperations.rangeWithScores("test_zset", 0, 4);
        test_zset.stream().forEach(x -> log.info("test_zset 中排名0-4 {}-{}", x.getValue(), x.getScore()));// a-0.0 b-1.0 c-2.0

//        zSetOperations.add("test_zset","c", 2);
//        zSetOperations.add("test_zset","d", 3);
//        zSetOperations.add("test_zset","f", 3);
        // 通过增量增加排序集中的元素的分数
        zSetOperations.incrementScore("test_zset", "c", 1);
        zSetOperations.incrementScore("test_zset", "d", 1);
        zSetOperations.incrementScore("test_zset", "f", 1);
        zSetOperations.incrementScore("test_zset", "g", 5);
        // 从排序集中获取开始和结束之间的元组（默认从小到大）。
        Set<ZSetOperations.TypedTuple<Object>> test_zset_range = zSetOperations.rangeWithScores("test_zset", -1 - 2, -1);
        test_zset_range.stream().forEach(x -> log.info("test_zset_range 中排名0-3 {}-{}", x.getValue(), x.getScore()));// d-4.0, f-4.0, g-5.0
        // 从高到低的排序集中获取分数在最小和最大值之间的元素（从大到小）。
        Set<ZSetOperations.TypedTuple<Object>> test_zset2_reverseRange = zSetOperations.reverseRangeWithScores("test_zset", 0, 2);
        test_zset2_reverseRange.stream().forEach(x -> log.info("test_zset2_reverseRange 中排名0-3 {}-{}", x.getValue(), x.getScore()));// g-5.0, f-4.0, d-4.0

        // mock 另外一个key
        zSetOperations.add("test_zset_o", "a", 3);
        zSetOperations.add("test_zset_o", "b", 2);
        zSetOperations.add("test_zset_o", "d", 1);

        // 取交集,并把结果存储到新的集合test_zset_intersectAndStore中
        Long aLong = zSetOperations.intersectAndStore("test_zset", "test_zset_o", "test_zset_intersectAndStore");
        zSetOperations.range("test_zset_intersectAndStore", 0, -1).stream().forEach(x -> log.info("取交集 {}", x));// a b d
        // 取全集,并把结果存储到新的集合test_zset_unionAndStore中
        Long aLong1 = zSetOperations.unionAndStore("test_zset", "test_zset_o", "test_zset_unionAndStore");
        zSetOperations.range("test_zset_unionAndStore", 0, -1).stream().forEach(x -> log.info("取全集 {}", x));// a b c f d g
    }

}
