package com.hcj.springcloud;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * 一个localCache的示例
 */
@Slf4j
public class LocalCacheTest {

    public static Map localCacheMap = new HashMap();

    public static Map db = new HashMap();

    public LocalCacheTest() {
        log.info("构造函数中的localCacheMap={}", localCacheMap);
    }

    static void init() {
        localCacheMap.put("b", "b");
        log.info("static中init后的localCacheMap={}", localCacheMap);
    }

    static Object get(String key) {
        if (localCacheMap.get(key) != null) {
            log.info("缓存有，则从缓存中获取{}", localCacheMap.get(key));
            return localCacheMap.get(key);
        } else {
            log.info("缓存无，则从db中获取, 并更新缓存{}", db.get(key));
            if (db.get(key) != null) {
                localCacheMap.put(key, db.get(key));
            }
            return db.get(key);
        }
    }

    static Object put(String key, Object data) {
        db.put(key, data);
        if (localCacheMap.get(key) != null) {
            localCacheMap.put(key, data);
            log.info("缓存有，则更新缓存,并且从缓存中获取{}", localCacheMap.get(key));
            return localCacheMap.get(key);
        } else {
            log.info("缓存无，则从数据中获取{}", db.get(key));
            return db.get(key);
        }
    }

    static {
        localCacheMap.put("a", "a");
        log.info("static中的localCacheMap={}", localCacheMap);
    }

    public static void main(String[] args) {
        LocalCacheTest localCacheTest = new LocalCacheTest();
        localCacheTest.init();
        localCacheTest.put("c", "c");

        Object cFromDb = localCacheTest.get("c");
        Object cFromLocalCache = localCacheTest.get("c");

    }
}
