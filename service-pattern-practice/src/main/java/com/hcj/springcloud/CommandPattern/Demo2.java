package com.hcj.springcloud.CommandPattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

/**
 * 命令模式-demo2
 * 通过使用命令模式，我们可以将命令与其执行者解耦，使得命令的请求者和执行者之间松耦合，提高了代码的灵活性和可维护性
 *
 * @author huangchangjin
 * @date 2023/11/28 15:20
 */
@Slf4j
public class Demo2 {
    static class VideoPlayer {
        public void play() {
            log.info("播放--");
        }

        public void stopPlay() {
            log.info("暂停-播放--");
        }

        public void closePlay() {
            log.info("关闭-播放--");
        }
    }

    interface Cmd {
        void execute();
    }

    static class PlayCmd implements Cmd {
        private VideoPlayer videoPlayer;

        public PlayCmd(VideoPlayer videoPlayer) {
            this.videoPlayer = videoPlayer;
        }

        @Override
        public void execute() {
            videoPlayer.play();
        }
    }

    static class StopCmd implements Cmd {
        private VideoPlayer videoPlayer;

        public StopCmd(VideoPlayer videoPlayer) {
            this.videoPlayer = videoPlayer;
        }

        @Override
        public void execute() {
            videoPlayer.stopPlay();
        }
    }

    static class CloseCmd implements Cmd {
        private VideoPlayer videoPlayer;

        public CloseCmd(VideoPlayer videoPlayer) {
            this.videoPlayer = videoPlayer;
        }

        @Override
        public void execute() {
            videoPlayer.closePlay();
        }
    }

    @Test
    void test_main() {
        VideoPlayer videoPlayer = new VideoPlayer();
        Cmd cmd = new PlayCmd(videoPlayer);
        cmd.execute();
        cmd = new StopCmd(videoPlayer);
        cmd.execute();
        cmd = new CloseCmd(videoPlayer);
        cmd.execute();
    }
}
