package com.hcj.springcloud.VisitorPattern;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

/**
 * 真实的访问者
 *
 * @author huangchangjin
 * @date 2024/7/9 16:16
 */
@Slf4j
@Data
public class ConcreteVisitor implements Visitor {
    private String name;

    public ConcreteVisitor() {
    }

    public ConcreteVisitor(String name) {
        this.name = name;
    }

    @Override
    public void visit(ElementA elementA) {
        elementA.featureA();
        elementA.featureAA();
        log.info("ElementA 需要打印当前时间 {}", LocalDateTime.now());
    }

    @Override
    public void visit(ElementB elementB) {
        elementB.featureB();
    }

    @Override
    public String getVisitorName() {
        return this.getName();
    }
}
