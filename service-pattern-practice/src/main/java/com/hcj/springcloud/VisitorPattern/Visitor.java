package com.hcj.springcloud.VisitorPattern;

/**
 * @author huangchangjin
 * @date 2024/7/9 16:11
 */
public interface Visitor {
    void visit(ElementA elementA);

    void visit(ElementB elementB);

    String getVisitorName();
}
