package com.hcj.springcloud.VisitorPattern;

/**
 * @author huangchangjin
 * @date 2024/7/9 16:12
 */
public interface Element {
    void accept(Visitor visitor);
}
