package com.hcj.springcloud.VisitorPattern;

import lombok.extern.slf4j.Slf4j;

/**
 * @author huangchangjin
 * @date 2024/7/9 16:13
 */
@Slf4j
public class ElementB implements Element {
    // 不破坏原有特性方法，例如
    void featureB() {
        log.info("ElementB 中特有的特性 B");
    }

    @Override
    public void accept(Visitor visitor) {
        log.info("ElementB 中的 visitor = {}", visitor.getVisitorName());
        visitor.visit(this);
    }
}
