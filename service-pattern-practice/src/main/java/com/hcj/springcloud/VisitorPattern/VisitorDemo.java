package com.hcj.springcloud.VisitorPattern;

import lombok.extern.slf4j.Slf4j;

/**
 * 如果你需要对一个复杂对象结构 （例如对象树） 中的所有元素执行某些操作， 可使用访问者模式。
 *
 * @author huangchangjin
 * @date 2024/7/9 16:19
 */
@Slf4j
public class VisitorDemo {
    public static void main(String[] args) {
        ConcreteVisitor concreteVisitor = new ConcreteVisitor("访问者A");
        ElementA elementA = new ElementA();
        ElementB elementB = new ElementB();
        elementA.accept(concreteVisitor);
        elementB.accept(concreteVisitor);

        // 只需要变动真实的 ConcreteVisitor 访问者，原始元素可以不用动，就能实现额外的功能


    }
}
