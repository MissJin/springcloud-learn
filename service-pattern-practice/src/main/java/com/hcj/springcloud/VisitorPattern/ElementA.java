package com.hcj.springcloud.VisitorPattern;

import lombok.extern.slf4j.Slf4j;

/**
 * @author huangchangjin
 * @date 2024/7/9 16:13
 */
@Slf4j
public class ElementA implements Element {
    // 不破坏原有特性方法，例如
    void featureA() {
        log.info("ElementA 中特有的特性 A");
    }

    void featureAA() {
        log.info("ElementA 中特有的特性 AA");
    }

    @Override
    public void accept(Visitor visitor) {
        log.info("ElementA 中的 visitor = {}", visitor.getVisitorName());
        visitor.visit(this);
    }
}
