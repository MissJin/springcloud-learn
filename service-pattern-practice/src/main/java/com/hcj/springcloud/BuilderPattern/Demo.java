package com.hcj.springcloud.BuilderPattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 建造者模式-demo
 * 举个例子：肯德基根据顾客点的餐，生成食物的过程
 *
 * @author huangchangjin
 * @date 2023/11/27 16:25
 */
@Slf4j
public class Demo {
    interface Packing {
        String pack();
    }

    static class Wrapper implements Packing {
        @Override
        public String pack() {
            return "纸袋包装";
        }
    }

    static class Bottle implements Packing {
        @Override
        public String pack() {
            return "瓶子包装";
        }
    }

    interface Item {
        String name();

        Packing packing();

        float price();
    }

    static class Meal {
        protected List<Item> foods = new ArrayList<>();

        void addItem(Item item) {
            foods.add(item);
        }

        void getCost() {
            float total = 0f;
            for (Item x : foods) {
                total += x.price();
            }
            log.info("总费用: {}", total);
        }

        void showItems() {
            for (Item x : foods) {
                log.info("name = {}, pack = {}, price = {}", x.name(), x.packing().pack(), x.price());
            }
        }
    }

    // 菜品
    static abstract class Burger implements Item {

        @Override
        public String name() {
            return "汉堡";
        }

        @Override
        public Packing packing() {
            return new Wrapper();
        }

        @Override
        public abstract float price();
    }

    static abstract class ColdDrink implements Item {

        @Override
        public String name() {
            return "冷饮";
        }

        @Override
        public Packing packing() {
            return new Bottle();
        }

        @Override
        public abstract float price();
    }

    static class VegBurger extends Burger {
        @Override
        public String name() {
            return "蔬菜汉堡";
        }

        @Override
        public float price() {
            return 15.0f;
        }
    }

    static class ChickenBurger extends Burger {
        @Override
        public String name() {
            return "鸡肉汉堡";
        }

        @Override
        public float price() {
            return 20.0f;
        }
    }

    static class PepsiDrink extends ColdDrink {

        @Override
        public String name() {
            return "百事-冷饮";
        }

        @Override
        public float price() {
            return 6.0f;
        }
    }

    static class CokeDrink extends ColdDrink {

        @Override
        public String name() {
            return "可口-冷饮";
        }

        @Override
        public float price() {
            return 7.0f;
        }
    }

    static class MealBuilder {
        static Meal prepareVegMeal(String type) {
            Meal meal = new Meal();
            if ("素食".equals(type)) {
                meal.addItem(new VegBurger());
                meal.addItem(new PepsiDrink());
            } else {
                meal.addItem(new ChickenBurger());
                meal.addItem(new CokeDrink());
            }
            return meal;
        }
    }

    @Test
    void test_main() {
        Meal meal1 = MealBuilder.prepareVegMeal("素食");
        meal1.showItems();
        meal1.getCost();


        Meal meal2 = MealBuilder.prepareVegMeal("非素食");
        meal2.showItems();
        meal2.getCost();
    }
}
