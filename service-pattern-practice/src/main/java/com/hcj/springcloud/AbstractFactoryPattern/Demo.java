package com.hcj.springcloud.AbstractFactoryPattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

/**
 * 虚拟工厂demo
 *
 * @author huangchangjin
 * @date 2023/11/24 17:17
 */
@Slf4j
public class Demo {
    interface Shape {
        void draw();
    }

    static class Rectangle implements Shape {
        @Override
        public void draw() {
            log.info("画长方形");
        }
    }

    static class Square implements Shape {
        @Override
        public void draw() {
            log.info("画正方形");
        }
    }

    static class RoundedSquare implements Shape {
        @Override
        public void draw() {
            log.info("画正方形-圆角");
        }
    }

    static class RoundedRectangle implements Shape {
        @Override
        public void draw() {
            log.info("画长方形-圆角");
        }
    }

    static abstract class AbstractFactory {
        abstract Shape getShape(String shapeType);
    }

    static class ShapeFactory extends AbstractFactory {
        @Override
        Shape getShape(String shapeType) {
            if ("长方形".equals(shapeType)) {
                return new RoundedRectangle();
            } else {
                return new RoundedSquare();
            }
        }
    }

    static class RoundedShapeFactory extends AbstractFactory {

        @Override
        Shape getShape(String shapeType) {
            if ("长方形".equals(shapeType)) {
                return new Rectangle();
            } else {
                return new Square();
            }
        }
    }

    static class FactoryProducer {
        static AbstractFactory getFactory(boolean isRounded) {
            if (!isRounded) {
                return new ShapeFactory();
            } else {
                return new RoundedShapeFactory();
            }
        }
    }

    @Test
    public void test_main() {
        AbstractFactory factory = FactoryProducer.getFactory(true);
        factory.getShape("长方形").draw();
        factory.getShape("正方形").draw();

        factory = FactoryProducer.getFactory(false);
        factory.getShape("长方形").draw();
        factory.getShape("正方形").draw();
    }


}
