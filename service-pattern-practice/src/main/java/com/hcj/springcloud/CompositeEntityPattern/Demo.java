package com.hcj.springcloud.CompositeEntityPattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 实体组合-demo
 * <pre>
 *     例如:一个文件系统, 由文件和文件夹组合合成的
 * </pre>
 *
 * @author huangchangjin
 * @date 2023/11/28 15:40
 */
@Slf4j
public class Demo {
    interface FileSystemComponent {
        void showInfo();
    }

    static class File implements FileSystemComponent {

        private String name;

        public File(String name) {
            this.name = name;
        }

        @Override
        public void showInfo() {
            log.info("文件信息 name = {}", name);
        }
    }

    static class Folder implements FileSystemComponent {

        private String name;
        private List<FileSystemComponent> list = new ArrayList<>();

        public Folder(String name) {
            this.name = name;
        }

        @Override
        public void showInfo() {
            log.info("文件夹-信息 name = {}", name);
            for (FileSystemComponent x : list) {
                log.info("|_{}", name);
                x.showInfo();
            }
        }

        /**
         * 添加文件组件（文件或者文件夹）
         */
        public void addFileComponent(FileSystemComponent fileSystemComponent) {
            list.add(fileSystemComponent);
        }
    }

    @Test
    void test_main() {
        File fileA = new File("a.txt");
        File fileB = new File("b.txt");
        Folder folderA = new Folder("文件夹A");
        Folder folderB = new Folder("文件夹B");

        folderA.addFileComponent(fileA);
        folderA.addFileComponent(folderB);

        folderB.addFileComponent(fileA);
        folderB.addFileComponent(fileB);

        folderA.showInfo();
    }
}
