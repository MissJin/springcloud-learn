package com.hcj.springcloud.AdapterPattern;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

/**
 * 适配器-demo
 *
 * @author huangchangjin
 * @date 2023/11/24 19:22
 */
@Slf4j
public class Demo {
    interface MediaPlayer {
        void play(String resourceName);
    }

    static class AudioPlayer implements MediaPlayer {
        MediaPlayerAdapter mediaPlayerAdapter;

        public AudioPlayer(MediaPlayerAdapter mediaPlayerAdapter) {
            this.mediaPlayerAdapter = mediaPlayerAdapter;
        }

        @Override
        public void play(String resourceName) {
            if (resourceName.contains("mp3")) {
                log.info("音频播放中。。。{}", resourceName);
            }
            if (mediaPlayerAdapter != null) {
                mediaPlayerAdapter.play(resourceName);
            }
        }
    }

    static class MediaPlayerAdapter implements MediaPlayer {
        AdvancedMediaPlayer advancedMediaPlayer;

        @Override
        public void play(String resourceName) {
            if (resourceName.contains("vlc")) {
                advancedMediaPlayer = new VlcPlayer();
                advancedMediaPlayer.playVlc();
            }
            if (resourceName.contains("mp4")) {
                advancedMediaPlayer = new MP4Player();
                advancedMediaPlayer.playMp4();
            }
        }
    }

    interface AdvancedMediaPlayer {
        void playVlc();

        void playMp4();
    }

    static class VlcPlayer implements AdvancedMediaPlayer {

        @Override
        public void playVlc() {
            log.info("同时播放vlc");
        }

        @Override
        public void playMp4() {
            log.info("无法播放mp4");
        }
    }

    static class MP4Player implements AdvancedMediaPlayer {

        @Override
        public void playVlc() {
            log.info("无法播放vlc");
        }

        @Override
        public void playMp4() {
            log.info("同时播放mp4");
        }
    }

    @Test
    public void test_main() {
        MediaPlayer mediaPlayer = new AudioPlayer(null);
        // 未加适配器的时候，只能播放mp3
        mediaPlayer.play("mp3|vlc|mp4");

        // 加了适配器，同样的资源文件，可以播放不同的东西
        mediaPlayer = new AudioPlayer(new MediaPlayerAdapter());
        mediaPlayer.play("mp3|vlc|mp4");
        mediaPlayer.play("mp3");
    }
}
