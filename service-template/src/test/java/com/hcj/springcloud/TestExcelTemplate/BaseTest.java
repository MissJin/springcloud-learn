package com.hcj.springcloud.TestExcelTemplate;

import junit.framework.TestCase;
import org.jxls.common.Context;
import org.jxls.template.SimpleExporter;
import org.jxls.util.JxlsHelper;

import java.io.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

/**
 * 测试:
 * 源码地址： https://github.com/jxlsteam/jxls.git 里面有详细的单元测试
 */
public class BaseTest extends TestCase {
    /**
     * 模板路径
     */
    private static final String EXCEL_TEMPLATE_BASE_PATH = "D:\\workspace-learn\\springcloud-learn\\service-template\\template\\excelTemplate";

    private List<Employee> initData() {
        List<Employee> employees = new ArrayList<>();
        IntStream.range(1, 5).forEach(i -> {
            Employee employee = new Employee("name" + i, LocalDateTime.now(), BigDecimal.TEN, BigDecimal.ONE);
            employees.add(employee);
            System.out.println("employee = " + employee);
        });
        return employees;
    }

    public void test_demo_a() throws IOException {
        List<Employee> employees = initData();
        processGridTemplateAtCell(employees, EXCEL_TEMPLATE_BASE_PATH, "template_a.xls", "template_output_a.xls");
    }

    public void test_demo_b() throws IOException {
        List<Employee> employees = initData();
        gridExport(employees, EXCEL_TEMPLATE_BASE_PATH, "template_b.xls", "template_output_b.xls");
    }

    private void gridExport(List<Employee> employees, String excelTemplatePath, String templateFileName, String outFileName) throws IOException {
        try (
                InputStream is = new FileInputStream(excelTemplatePath + File.separator + templateFileName);
                OutputStream os = new FileOutputStream(excelTemplatePath + File.separator + outFileName);
        ) {
            SimpleExporter exporter = new SimpleExporter();
            exporter.registerGridTemplate(is);
            List<String> headers = Arrays.asList("Name", "Birthday", "Payment", "bonus");
            exporter.gridExport(headers, employees, "name, birthDate, payment, bonus", os);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processGridTemplateAtCell(List<Employee> employees, String excelTemplatePath, String templateFileName, String outFileName) throws IOException {
        try (
                InputStream is = new FileInputStream(excelTemplatePath + File.separator + templateFileName);
                OutputStream os = new FileOutputStream(excelTemplatePath + File.separator + outFileName);
        ) {
            Context context = new Context();
            context.putVar("headers", Arrays.asList("Name", "Birthday", "Payment", "bonus"));
            context.putVar("data", employees);
            JxlsHelper instance = JxlsHelper.getInstance();
            instance.processGridTemplateAtCell(is, os, context, "name,birthDate,payment,bonus", "resultSheet!A1");
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }
}
