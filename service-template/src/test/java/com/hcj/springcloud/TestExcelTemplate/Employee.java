package com.hcj.springcloud.TestExcelTemplate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
    private String name;
    private LocalDateTime birthDate;
    private BigDecimal payment;
    private BigDecimal bonus;

}