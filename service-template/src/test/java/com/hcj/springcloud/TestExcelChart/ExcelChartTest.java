package com.hcj.springcloud.TestExcelChart;

import com.aspose.cells.*;
import junit.framework.TestCase;

/**
 * 【参考】https://gist.github.com/aspose-com-gists/f5d1733288db72c17d541ac5871cf15d#file-create-column-chart-java
 */
public class ExcelChartTest extends TestCase {

    /**
     * 【柱状图】
     */
    public void test_column_chart() throws Exception {
        // Create a new workbook
        Workbook workbook = new Workbook();

        // Obtain the reference of the first worksheet
        Worksheet worksheet = workbook.getWorksheets().get(0);

        // Add sample values to cells
        worksheet.getCells().get("A2").putValue("Category1");
        worksheet.getCells().get("A3").putValue("Category2");
        worksheet.getCells().get("A4").putValue("Category3");

        worksheet.getCells().get("B1").putValue("Column1");
        worksheet.getCells().get("B2").putValue(4);
        worksheet.getCells().get("B3").putValue(20);
        worksheet.getCells().get("B4").putValue(50);
        worksheet.getCells().get("C1").putValue("Column2");
        worksheet.getCells().get("C2").putValue(50);
        worksheet.getCells().get("C3").putValue(100);
        worksheet.getCells().get("C4").putValue(150);

        // Add a chart to the worksheet
        int chartIndex = worksheet.getCharts().add(ChartType.COLUMN, 5, 0, 15, 5);

        // Access the instance of the newly added chart
        Chart chart = worksheet.getCharts().get(chartIndex);

        // Set chart data source as the range "A1:C4"
        chart.setChartDataRange("A1:C4", true);
        workbook.save("Column-Chart.xlsx", SaveFormat.XLSX);
    }

    /**
     * 【折线图】
     */
    public void test_line_chart() throws Exception {
        // Instantiate a Workbook object
        Workbook workbook = new Workbook();

        // Obtain the reference of the first worksheet
        Worksheet worksheet = workbook.getWorksheets().get(0);

        // Add sample values to cells
        worksheet.getCells().get("A2").putValue("Category1");
        worksheet.getCells().get("A3").putValue("Category2");
        worksheet.getCells().get("A4").putValue("Category3");

        worksheet.getCells().get("B1").putValue("Column1");
        worksheet.getCells().get("B2").putValue(4);
        worksheet.getCells().get("B3").putValue(20);
        worksheet.getCells().get("B4").putValue(50);
        worksheet.getCells().get("C1").putValue("Column2");
        worksheet.getCells().get("C2").putValue(50);
        worksheet.getCells().get("C3").putValue(100);
        worksheet.getCells().get("C4").putValue(150);

        // Add a chart to the worksheet
        int chartIndex = worksheet.getCharts().add(ChartType.LINE, 5, 0, 15, 5);

        // Access the instance of the newly added chart
        Chart chart = worksheet.getCharts().get(chartIndex);

        // Set chart data source as the range "A1:C4"
        chart.setChartDataRange("A1:C4", true);

        // Save the Excel file
        workbook.save("Line-Chart.xls", SaveFormat.XLSX);
    }

    /**
     * 【无表头的柱状图】
     */
    public void test_pyramid_chart() throws Exception {
        // Instantiate a Workbook object
        Workbook workbook = new Workbook();

        // Obtain the reference of the first worksheet
        WorksheetCollection worksheets = workbook.getWorksheets();
        Worksheet sheet = worksheets.get(0);

        // Add some sample value to cells
        Cells cells = sheet.getCells();
        Cell cell = cells.get("A1");
        cell.setValue(50);
        cell = cells.get("A2");
        cell.setValue(100);
        cell = cells.get("A3");
        cell.setValue(150);
        cell = cells.get("B1");
        cell.setValue(4);
        cell = cells.get("B2");
        cell.setValue(20);
        cell = cells.get("B3");
        cell.setValue(180);
        cell = cells.get("C1");
        cell.setValue(320);
        cell = cells.get("C2");
        cell.setValue(110);
        cell = cells.get("C3");
        cell.setValue(180);
        cell = cells.get("D1");
        cell.setValue(40);
        cell = cells.get("D2");
        cell.setValue(120);
        cell = cells.get("D3");
        cell.setValue(250);

        // Access chart collection
        ChartCollection charts = sheet.getCharts();

        // Add a chart to the worksheet
        int chartIndex = charts.add(ChartType.PYRAMID, 5, 0, 15, 5);
        Chart chart = charts.get(chartIndex);

        // Add NSeries (chart data source) to the chart ranging from "A1"
        // cell to "B3"
        SeriesCollection serieses = chart.getNSeries();
        serieses.add("A1:B3", true);

        // Save the Excel file
        workbook.save("Pyramid-Chart.xlsx", SaveFormat.XLSX);
    }

    /**
     * 【饼图】
     */
    public void test_pie_chart() throws Exception {
        // todo: 待完善
    }


}
