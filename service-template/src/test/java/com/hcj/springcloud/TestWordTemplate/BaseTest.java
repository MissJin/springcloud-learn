package com.hcj.springcloud.TestWordTemplate;

import com.deepoove.poi.XWPFTemplate;
import junit.framework.TestCase;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

/**
 * 测试:
 * 官方文档：http://deepoove.com/poi-tl/1.6.x
 * 源码地址： https://github.com/Sayi/poi-tl.git 里面有详细的单元测试
 */
public class BaseTest extends TestCase {
    /**
     * 模板路径
     */
    String wordTemplatePath = "D:\\workspace-learn\\springcloud-learn\\service-template\\template\\wordTemplate";

    public void test_demo_a() throws IOException {
        //Map<String, String> getenv = System.getenv();
        String wordTemplateFile = wordTemplatePath + File.separator + "template.docx";
        XWPFTemplate template = XWPFTemplate.compile(wordTemplateFile)
                .render(new HashMap<String, Object>() {{
                    put("title", "Poi-tl 模板引擎");
                }});
        FileOutputStream out = new FileOutputStream(wordTemplatePath + File.separator + "out_template.docx");
        template.write(out);
        out.flush();
        out.close();
        template.close();
    }
}
