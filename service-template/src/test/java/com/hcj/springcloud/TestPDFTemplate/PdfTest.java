package com.hcj.springcloud.TestPDFTemplate;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import junit.framework.TestCase;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * itext测试
 */
public class PdfTest extends TestCase {
    /**
     * 模板路径
     */
    private static final String PDF_TEMPLATE_BASE_PATH = "D:\\workspace-learn\\springcloud-learn\\service-template\\template\\pdfTemplate";

    public void test_pdf() throws FileNotFoundException {
        PdfDocument pdf = new PdfDocument(new PdfWriter(PDF_TEMPLATE_BASE_PATH + File.separator + "hello.pdf"));
        Document document = new Document(pdf);
        String line = "Hello! Welcome to iTextPdf";
        document.add(new Paragraph(line));
        document.close();

        System.out.println("Awesome PDF just got created.");
    }
}
