package com.hcj.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceTemplateApp {

    public static void main(String[] args) {
        SpringApplication.run(ServiceTemplateApp.class, args);
    }

}
