package com.hcj.springcloud.controller;

import com.hcj.springcloud.entity.EduCategory;
import com.hcj.springcloud.entity.EduCourse;
import com.hcj.springcloud.service.EduCategoryService;
import com.hcj.springcloud.service.EduCourseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/app/eduCourse")
public class EduCourseController {
    @Autowired
    private EduCourseService eduCourseService;
    @Autowired
    private MongoTemplate mongoTemplate;

    @GetMapping("/{id}")
    public Object test(@PathVariable("id") Long id){
        Date date = new Date();
        EduCategory eduCategory = new EduCategory();
        eduCategory.setId(id);
        eduCategory.setName("test");
        eduCategory.setCreateTime(date);
        eduCategory.setUpdateTime(date);
        eduCategory.setCreateUserId(1L);
        eduCategory.setUpdateUserId(2L);

        EduCourse eduCourse = new EduCourse();
        eduCourse.setId(id);
        eduCourse.setName("课程名称");
        eduCourse.setAbout("这是简介");
        eduCourse.setCoverImg("这是封面地址");
        eduCourse.setDuration(2D);
        eduCourse.setOrderNum(0);
        eduCourse.setCreateTime(date);
        eduCourse.setUpdateTime(date);
        eduCourse.setCreateUserId(1L);
        eduCourse.setUpdateUserId(2L);
        eduCourse.setCategoryId(eduCategory);
        eduCourseService.save(eduCourse);
        EduCourse result = eduCourseService.queryById(id);
        log.info("result :{}",result);
        return result;
    }


    @GetMapping("/getPage")
    public Object getPage(EduCourse eduCourse,int start, int size){
        List<EduCourse> page = eduCourseService.getPage(eduCourse, start, size);
        log.info("test auto run...");
        log.info("page is :{}", page);
        return page;
    }

    @GetMapping("/getByMongoTemplate")
    public Object getByMongoTemplate(){
        Query query = new Query();
        // 模拟：按categoryId.name来分组
        // db.eduCourse.distinct('categoryId.name');
        List<Object> distinct = mongoTemplate.findDistinct(query, "categoryId.name", EduCourse.class, Object.class);
        log.info(" distinct is :{}",distinct);
        return distinct;
    }


    @GetMapping("/update")
    public Object update(){
        Query query = new Query();
        // 模拟：按categoryId.name来分组
        // ddb.getCollection("eduCourse").update( { _id: NumberLong("2") }, { $set: { "categoryId.name": "test_2" } } )
        EduCourse eduCourse = new EduCourse();
        EduCategory eduCategory = new EduCategory();
        eduCategory.setName("test_1");
        eduCourse.setId(2L);
        //eduCourse.setCategoryId(eduCategory);
        Criteria criteria = new Criteria();
//        criteria.and("_id").is(2);
        criteria.where("_id").is(2);
        query.addCriteria(criteria);
        List<EduCourse> eduCourses = mongoTemplate.find(query, EduCourse.class);

        log.info(" eduCourses is :{}", eduCourses);
        return eduCourses;
    }


}
