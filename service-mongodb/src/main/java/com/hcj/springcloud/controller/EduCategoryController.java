package com.hcj.springcloud.controller;

import com.hcj.springcloud.entity.EduCategory;
import com.hcj.springcloud.service.EduCategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@Slf4j
@RestController
@RequestMapping("/app/eduCategory")
public class EduCategoryController {
    @Autowired
    private EduCategoryService eduCategoryService;

    @GetMapping("/{id}")
    public Object test(@PathVariable("id") Long id){
        Date date = new Date();
        EduCategory eduCategory = new EduCategory();
        eduCategory.setId(id);
        eduCategory.setName("test");
        eduCategory.setCreateTime(date);
        eduCategory.setUpdateTime(date);
        eduCategory.setCreateUserId(1L);
        eduCategory.setUpdateUserId(2L);
        eduCategoryService.save(eduCategory);
        return eduCategoryService.queryById(id);
    }
}
