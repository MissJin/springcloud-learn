package com.hcj.springcloud.service;

import com.hcj.springcloud.dao.EduCategoryDao;
import com.hcj.springcloud.dao.EduCourseDao;
import com.hcj.springcloud.entity.EduCategory;
import com.hcj.springcloud.entity.EduCourse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EduCourseService {
    @Autowired
    private EduCourseDao eduCourseDao;

    public void save(EduCourse entity){
        eduCourseDao.save(entity);
    }

    public EduCourse queryById(Long id){
        return eduCourseDao.queryById(id);
    }

    public List<EduCourse> getPage(EduCourse eduCourse, int start, int size){
        return eduCourseDao.getPage(eduCourse, start, size);
    }
}
