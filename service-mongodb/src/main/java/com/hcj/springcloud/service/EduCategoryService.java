package com.hcj.springcloud.service;

import com.hcj.springcloud.dao.EduCategoryDao;
import com.hcj.springcloud.entity.EduCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EduCategoryService {
    @Autowired
    private EduCategoryDao eduCategoryDao;

    public void save(EduCategory entity){
        eduCategoryDao.save(entity);
    }

    public EduCategory queryById(Long id){
        return eduCategoryDao.queryById(id);
    }
}
