package com.hcj.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableEurekaClient
public class ServiceMongodbApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceMongodbApplication.class, args);
    }

}
