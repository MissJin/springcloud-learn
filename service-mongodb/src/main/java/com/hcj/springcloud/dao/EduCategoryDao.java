package com.hcj.springcloud.dao;

import com.hcj.springcloud.entity.EduCategory;
import org.springframework.stereotype.Repository;

@Repository
public class EduCategoryDao extends MongoDbDao<EduCategory>{
    @Override
    protected Class<EduCategory> getEntityClass() {
        return EduCategory.class;
    }
}
