package com.hcj.springcloud.dao;

import com.hcj.springcloud.entity.EduCourse;
import org.springframework.stereotype.Repository;

@Repository
public class EduCourseDao extends MongoDbDao<EduCourse>{
    @Override
    protected Class<EduCourse> getEntityClass() {
        return EduCourse.class;
    }
}
