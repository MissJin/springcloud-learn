package com.hcj.springcloud.entity;

import lombok.Data;

import java.util.Date;

@Data
public class EduCategory {
    private Long id;
    private String name;
    private Integer orderNum;
    private Long createUserId;
    private Long updateUserId;
    private Date createTime;
    private Date updateTime;
}
