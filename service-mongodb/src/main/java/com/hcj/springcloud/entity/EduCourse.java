package com.hcj.springcloud.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document
public class EduCourse {
//    `id` bigint(20) NOT NULL COMMENT 'ID',
//            `name` varchar(50) DEFAULT NULL COMMENT '名称',
//            `cover_img` varchar(255) DEFAULT NULL COMMENT '课程封面',
//            `category_id` bigint(20) DEFAULT NULL COMMENT '课程类型',
//            `duration` double DEFAULT NULL COMMENT '培训时长',
//            `about` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT '培训简介',
//            `order_num` int(11) DEFAULT NULL COMMENT '排序值',
//            `status` tinyint(2) DEFAULT NULL COMMENT '发布状态：0未发布1已发布',
//            `create_user` bigint(20) DEFAULT NULL COMMENT '创建人',
//            `update_user` bigint(20) DEFAULT NULL COMMENT '修改人',
//            `create_time` datetime DEFAULT NULL COMMENT '创建时间',
//            `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    private Long id;
    private String name;
    private String coverImg;
    private Double duration;
    private String about;
    private Integer orderNum;
    private Integer status;
    private Long createUserId;
    private Long updateUserId;
    private Date createTime;
    private Date updateTime;
    private EduCategory categoryId;
}
