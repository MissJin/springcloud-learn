### websocket + springboot2 
- 实现的单人聊天和多人聊天以及广播的功能

### 用法
- 1 进入 localhost:8080/
- 2 登入
- 3 发消息

### 效果图
- 在同一个聊天组内聊天的效果

![聊天组1聊天1.png](screenshot/聊天组聊天1.png)
***
![聊天组1聊天2.png](screenshot/聊天组聊天2.png)