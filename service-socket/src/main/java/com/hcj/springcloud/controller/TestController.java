package com.hcj.springcloud.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Slf4j
@RestController
public class TestController {

    /**
     * 登陆界面
     */
    @GetMapping("/")
    public ModelAndView login() {
        return new ModelAndView("/login");
    }

    /**
     * 聊天界面
     */
    @GetMapping("/index")
    public ModelAndView index(String username, String password,String groupId, HttpServletRequest request) throws UnknownHostException {
        if (StringUtils.isEmpty(username)) {
            username = "匿名用户";
        }
        ModelAndView mav = new ModelAndView("/chat");
        mav.addObject("username", username);
        StringBuilder sb = new StringBuilder();
        sb.append("ws://")
                .append(InetAddress.getLocalHost().getHostAddress())
                .append(":")
                .append(request.getServerPort())
                .append(request.getContextPath())
                .append("/chat/")
                .append(username)
                .append("/")
                .append(groupId);
        log.info("webSocketUrl={}", sb.toString());
        mav.addObject("webSocketUrl", sb.toString());
        mav.addObject("groupId", groupId);
        return mav;
    }
}
