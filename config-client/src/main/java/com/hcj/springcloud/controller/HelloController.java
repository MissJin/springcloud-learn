package com.hcj.springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@lombok.extern.slf4j.Slf4j
public class HelloController {
    @Value("${foo}")
    private String foo;
    @Value("${spring.application.name}")
    private String name;
    @Value("${spring.cloud.config.uri}")
    private String uri;

    @GetMapping("/hi")
    public String hi(){
        log.info("服务名称：{} \t 远程服务名：{}中，config-client-dev.properties里面的 foo:{}", name, uri, foo);
        return foo;
    }
}
