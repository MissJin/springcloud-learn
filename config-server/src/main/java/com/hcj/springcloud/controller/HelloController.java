package com.hcj.springcloud.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@lombok.extern.slf4j.Slf4j
@RequestMapping("/")
public class HelloController {
}
