## 启动服务 config-server后
> 正确启动后，访问 [http://localhost:8888/address/dev/master](http://localhost:8888/address/dev/master)

```$xslt
{
    "name": "address",
    "profiles": [
        "dev"
    ],
    "label": "master",
    "version": "0fc8081c507d694b27967e9074127b373d196431",
    "state": null,
    "propertySources": [
        {
            "name": "https://github.com/MissJin/SpringcloudConfig.git/respo/address-dev.properties",
            "source": {
                "spring.rabbitmq.host": "localhost",
                "spring.rabbitmq.port": "5672",
                "server.port": "8882",
                "hystrix.command.default.execution.timeout.enabled": "false ",
                "management.security.enabled": "false"
            }
        }
    ]
}
```
> http请求地址和资源文件映射如下:
-  /{application}/{profile}[/{label}]
-  /{application}-{profile}.yml
-  /{label}/{application}-{profile}.yml
-  /{application}-{profile}.properties
-  /{label}/{application}-{profile}.properties