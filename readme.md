## eureka
> 服务注册中心

## 准备工作
> 启动eureka-server 工程；
启动service-hi工程，它的端口为8762；
将service-hi的配置文件的端口改为8763,并启动，
这时你会发现：service-hi在eureka-server注册了2个实例，这就相当于一个小的集群。访问localhost:8761


## 进展[备注]
`springcloud的搭建`
- A:注册发现：eureka
- B:负载均衡：ribbon/feign
- C:熔断器：hystrix
- D:以及熔断器的可视化运维监控：hystrix-dashboard
- E:路由转发和过滤器:zuul		[即可以路由到哪个负载均衡器（ribbon/feign）,做一些请求的过滤]
- F:服务的配置:cloud-config