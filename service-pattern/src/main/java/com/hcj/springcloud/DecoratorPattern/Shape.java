package com.hcj.springcloud.DecoratorPattern;

public interface Shape {
    void draw();
}
