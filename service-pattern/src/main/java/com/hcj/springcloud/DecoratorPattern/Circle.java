package com.hcj.springcloud.DecoratorPattern;

public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println("Circle::draw() method.");
    }
}
