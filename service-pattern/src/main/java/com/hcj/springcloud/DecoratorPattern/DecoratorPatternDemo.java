package com.hcj.springcloud.DecoratorPattern;

/**
 * 装饰器模式：
 * 装饰器模式允许用户在不改变其结构的情况下向现有对象添加新功能。
 * 这种类型的设计模式属于结构模式，因为此模式充当现有类的包装器。
 *
 * 此模式创建一个装饰器类，它包装原始类并提供其他功能，使类方法签名保持不变。
 *
 * 我们通过以下示例展示了装饰器模式的使用，其中我们将使用一些颜色装饰一个没有改变形状类的形状。
 */
public class DecoratorPatternDemo {
    public static void main(String[] args) {
        Shape circle = new Circle();

        Shape redCircle = new RedShapeDecorator(new Circle());

        Shape redRectangle = new RedShapeDecorator(new Rectangle());
        System.out.println("正常圆形。。。");
        circle.draw();

        System.out.println("\n红色圆形。。。");
        redCircle.draw();

        System.out.println("\n红色长方形。。。");
        redRectangle.draw();
    }
}
