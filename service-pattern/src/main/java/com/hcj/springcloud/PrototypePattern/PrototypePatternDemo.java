package com.hcj.springcloud.PrototypePattern;

/**
 * 原型模式是指在保持性能的同时创建重复对象。这种类型的设计模式属于创建模式，因为此模式提供了创建对象的最佳方法之一。
 * 此模式涉及实现原型接口，该接口告诉创建当前对象的克隆。当直接创建对象成本高时使用此模式。
 * 例如，在昂贵的数据库操作之后创建对象。我们可以缓存对象，在下一个请求时返回其克隆，并在需要时更新数据库，从而减少数据库调用
 */
public class PrototypePatternDemo {
    public static void main(String[] args) {
        ShapeCache.loadCache();

        Shape shape1 = ShapeCache.getShape("1");
        System.out.println("Shape:"+ shape1.getType());
        shape1.draw();
        Shape shape2 = ShapeCache.getShape("2");
        System.out.println("Shape:"+ shape2.getType());
        shape2.draw();
        Shape shape3= ShapeCache.getShape("3");
        System.out.println("Shape:"+ shape3.getType());
        shape3.draw();
    }
}
