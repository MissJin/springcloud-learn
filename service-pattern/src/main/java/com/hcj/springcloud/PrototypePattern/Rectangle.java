package com.hcj.springcloud.PrototypePattern;

/**
 * 长方体
 */
public class Rectangle extends Shape {

    public Rectangle() {
        type = "Rectangle";
    }

    @Override
    void draw() {
        System.out.println("Rectangle:: draw() method.");
    }
}
