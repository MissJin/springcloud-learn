package com.hcj.springcloud.AdapterPattern;

/**
 * 适配器模式：
 * @des
 * 适配器模式充当两个不兼容接口之间的桥梁。这种类型的设计模式属于结构模式，因为该模式结合了两个独立接口的功能。
 * 此模式涉及单个类，该类负责连接独立或不兼容接口的功能。一个真实的例子可能是读卡器的情况，它充当存储卡和笔记本电脑之间的适配器。您将存储卡插入读卡器和读卡器插入笔记本电脑，以便通过笔记本电脑读取存储卡
 */
public class AdapterPatternDemo {

    public static void main(String[] args) {
        AudioPlayer audioPlayer = new AudioPlayer();
        audioPlayer.play("mp3","beyond-喜欢你.mp3");
        audioPlayer.play("mp4","楚门的世界.mp4");
        audioPlayer.play("vlc","纯音频.vlc");
        audioPlayer.play("rmvb","视频.rmvb");
    }
}
