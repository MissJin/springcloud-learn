package com.hcj.springcloud.AdapterPattern;

/**
 * 音频播放器：通过适配器后，它不仅能播放音频，还能播放视频
 */
public class AudioPlayer implements MediaPlayer {
    MediaAdapter mediaAdapter;

    @Override
    public void play(String audioType, String fileName) {
        if (audioType.equalsIgnoreCase("mp3")) {
            System.out.println("AudioPlayer play mp3 file:" + fileName);
        } else if (audioType.equalsIgnoreCase("vlc")
                || audioType.equalsIgnoreCase("mp4")) {
            mediaAdapter = new MediaAdapter(audioType);
            mediaAdapter.play(audioType, fileName);
        } else {
            System.out.println("invalid media. " + audioType + "format not supported.");
        }
    }
}
