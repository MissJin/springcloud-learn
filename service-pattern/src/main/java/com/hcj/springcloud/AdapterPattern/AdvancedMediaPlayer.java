package com.hcj.springcloud.AdapterPattern;

/**
 * 高级媒体播放器：接口
 */
public interface AdvancedMediaPlayer {
    void playVlc(String fileName);
    void playMp4(String fileName);
}
