package com.hcj.springcloud.AdapterPattern;

/**
 * 音频播放器：只能播放音频
 */
public class VlcPlayer implements AdvancedMediaPlayer{
    @Override
    public void playVlc(String fileName) {
        System.out.println("VlcPlayer: 音频播放器播放名字为："+ fileName);
    }

    @Override
    public void playMp4(String fileName) {
        //do nothing
    }
}
