package com.hcj.springcloud.AdapterPattern;

/**
 * 多媒体播放器接口
 */
public interface MediaPlayer {
    void play(String audioType, String fileName);
}
