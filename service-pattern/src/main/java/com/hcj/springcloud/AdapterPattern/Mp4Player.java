package com.hcj.springcloud.AdapterPattern;

/**
 * mp4播放器：只能播放视频
 */
public class Mp4Player implements AdvancedMediaPlayer{
    @Override
    public void playVlc(String fileName) {
        //System.out.println("Mp4Player: mp4播放器播放名字为："+ fileName+" 的音频");
    }

    @Override
    public void playMp4(String fileName) {
        System.out.println("Mp4Player: mp4播放器播放名字为："+ fileName+" 的视频");
    }
}
