package com.hcj.springcloud.TemplatePattern;

/**
 * 抽象类： 游戏
 */
public abstract class Game {
    public abstract void initialize();
    public abstract void startPlay();
    public abstract void endPlay();


    // template method[每个游戏都是这个流程]
    public final void play(){
        initialize();
        startPlay();
        endPlay();
    }

}
