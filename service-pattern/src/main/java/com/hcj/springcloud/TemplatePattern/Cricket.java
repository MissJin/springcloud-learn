package com.hcj.springcloud.TemplatePattern;

/**
 * 橄榄球-游戏
 */
public class Cricket extends Game {
    @Override
    public void initialize() {
        System.out.println("Cricket::initialize() 橄榄球-游戏 初始化");
    }

    @Override
    public void startPlay() {
        System.out.println("Cricket::startPlay() 橄榄球-游戏 游戏开始");
    }

    @Override
    public void endPlay() {
        System.out.println("Cricket::endPlay() 橄榄球-游戏 游戏结束");
    }
}
