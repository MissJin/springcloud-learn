package com.hcj.springcloud.TemplatePattern;

/**
 * 足球-游戏
 */
public class Football extends Game {
    @Override
    public void initialize() {
        System.out.println("Football::initialize() 足球-游戏 初始化");
    }

    @Override
    public void startPlay() {
        System.out.println("Football::startPlay() 足球-游戏 游戏开始");
    }

    @Override
    public void endPlay() {
        System.out.println("Football::endPlay() 足球-游戏 游戏结束");
    }
}
