package com.hcj.springcloud.TemplatePattern;
/**
 * 模板模式：
 * <br>
 * 在模板模式中，抽象类公开定义的方式/模板以执行其方法。
 * 它的子类可以根据需要覆盖方法实现，但调用的方式与抽象类的定义相同。
 * 此模式属于行为模式类别。
 * <br>---------------概览-----------------<br>
 * 我们将创建一个Game抽象类，定义操作，模板方法设置为final，以便不能覆盖它。
 * 板球和足球是扩展游戏并覆盖其方法的具体类。
 * 我们的演示类TemplatePatternDemo将使用Game来演示模板模式的使用
 */
public class TemplatePatternDemo {
    public static void main(String[] args) {
        Game cricket = new Cricket();
        cricket.play();

        Game football = new Football();
        football.play();
        //        Cricket::initialize() 橄榄球-游戏 初始化
        //        Cricket::startPlay() 橄榄球-游戏 游戏开始
        //        Cricket::endPlay() 橄榄球-游戏 游戏结束
        //        Football::initialize() 足球-游戏 初始化
        //        Football::startPlay() 足球-游戏 游戏开始
        //        Football::endPlay() 足球-游戏 游戏结束

    }
}
