package com.hcj.springcloud.ProxyPattern;

public class RealImage implements Image {
    private String fileName;

    public RealImage(String fileName) {
        this.fileName = fileName;
        loadFromDisk(fileName);
    }

    private void loadFromDisk(String fileName) {
        System.out.println("loading from disk:"+ fileName);
    }

    @Override
    public void display() {
        System.out.println("RealImage::display() method. and image is :"+ fileName);
    }
}
