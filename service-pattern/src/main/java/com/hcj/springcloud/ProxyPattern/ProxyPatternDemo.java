package com.hcj.springcloud.ProxyPattern;

/**
 * 代理模式：
 * 在代理模式中，类表示另一个类的功能。这种类型的设计模式属于结构模式。
 *
 * 在代理模式中，我们创建具有原始对象的对象以将其功能与外部世界接口。
 */
public class ProxyPatternDemo {
    public static void main(String[] args) {
        ProxyImage proxyImage = new ProxyImage("美女.png");
        System.out.println("第一次展示图片：会再从磁盘中加载");
        proxyImage.display();
        System.out.println("第二次展示图片：不会再从磁盘中加载");
        proxyImage.display();
    }
}
