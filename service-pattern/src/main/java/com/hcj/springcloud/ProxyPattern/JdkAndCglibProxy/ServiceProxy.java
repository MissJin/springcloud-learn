package com.hcj.springcloud.ProxyPattern.JdkAndCglibProxy;

/**
 * Service代理类
 */
public class ServiceProxy implements IService {
    /**
     * 目标代理对象
     */
    private IService target;

    public ServiceProxy(IService target) {
        this.target = target;
    }

    @Override
    public String m1() {
        long start = System.currentTimeMillis();
        String s = this.target.m1();
        long end = System.currentTimeMillis();
        System.out.println(this.target.getClass() + ".m1() 耗时：" + (end - start));
        return s;
    }

    @Override
    public String m2() {
        long start = System.currentTimeMillis();
        String s = this.target.m2();
        long end = System.currentTimeMillis();
        System.out.println(this.target.getClass() + ".m2() 耗时：" + (end - start));
        return s;
    }

    @Override
    public String m3() {
        long start = System.currentTimeMillis();
        String s = this.target.m3();
        long end = System.currentTimeMillis();
        System.out.println(this.target.getClass() + ".m3() 耗时：" + (end - start));
        return s;
    }
}
