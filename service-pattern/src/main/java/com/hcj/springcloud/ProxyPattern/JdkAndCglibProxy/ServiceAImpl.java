package com.hcj.springcloud.ProxyPattern.JdkAndCglibProxy;

public class ServiceAImpl implements IService {
    @Override
    public String m1() {
        System.out.println("this is serviceA中 m1");
        return "ServiceAImpl.m1()";
    }

    @Override
    public String m2() {
        System.out.println("this is serviceA中 m2");
        return "ServiceAImpl.m2()";
    }

    @Override
    public String m3() {
        System.out.println("this is serviceA中 m3");
        return "ServiceAImpl.m3()";
    }
}
