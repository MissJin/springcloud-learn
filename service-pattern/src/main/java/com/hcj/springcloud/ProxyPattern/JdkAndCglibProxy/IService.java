package com.hcj.springcloud.ProxyPattern.JdkAndCglibProxy;

public interface IService {
    String m1();

    String m2();

    String m3();
}
