package com.hcj.springcloud.ProxyPattern.JdkAndCglibProxy;

public class ServiceBImpl implements IService {
    @Override
    public String m1() {
        System.out.println("this is serviceB中 m1");
        return "ServiceBImpl.m1()";
    }

    @Override
    public String m2() {
        System.out.println("this is serviceB中 m2");
        return "ServiceBImpl.m2()";
    }

    @Override
    public String m3() {
        System.out.println("this is serviceB中 m3");
        return "ServiceBImpl.m3()";
    }
}
