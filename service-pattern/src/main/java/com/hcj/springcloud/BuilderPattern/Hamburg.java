package com.hcj.springcloud.BuilderPattern;

/**
 * 汉堡类的食物
 */
public abstract class Hamburg implements FoodItem {

    @Override
    public FoodPacking packing() {
        return new Wrapper();
    }

    @Override
    public abstract float price();
}
