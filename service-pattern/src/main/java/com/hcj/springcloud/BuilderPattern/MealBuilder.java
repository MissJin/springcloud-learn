package com.hcj.springcloud.BuilderPattern;

/**
 * 生成一顿饭
 */
public class MealBuilder {
    /**
     * 创建一顿：素餐
     * @return
     */
    public Meal createVegMeal(){
        Meal meal = new Meal();
        meal.addFoodItem(new VegHamburg());
        meal.addFoodItem(new CokeColdDrink());
        return meal;
    }

    /**
     * 创建一顿：非素餐
     * @return
     */
    public Meal createNoVegMeal(){
        Meal meal = new Meal();
        meal.addFoodItem(new ChickenHamburg());
        meal.addFoodItem(new PepsiColdDrink());
        return meal;
    }

}
