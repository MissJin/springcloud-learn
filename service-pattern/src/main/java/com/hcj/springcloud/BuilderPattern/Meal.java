package com.hcj.springcloud.BuilderPattern;

import java.util.ArrayList;
import java.util.List;

/**
 * 一顿饭
 */
public class Meal {
    private List<FoodItem> foodItems = new ArrayList<>();

    /**
     * 添加食物
     *
     * @param foodItem
     */
    public void addFoodItem(FoodItem foodItem) {
        foodItems.add(foodItem);
    }

    /**
     * 结算
     *
     * @return
     */
    public float getCost() {
        float cost = 0f;
        for (FoodItem foodItem : foodItems) {
            cost += foodItem.price();
        }
        return cost;
    }

    /**
     * 查看某一餐的具体
     */
    public void showItems() {
        for (FoodItem foodItem : foodItems) {
            //System.out.printf("foodName:%8s" + foodItem.name() + "\t\t|\t" + "foodPacking:\t" + foodItem.packing().pack() + "\t\t|\t" + "foodPrice:\t" + foodItem.price());
            System.out.printf("foodName: %30s \t foodPacking: %10s \tfoodPrice: %5s",foodItem.name(),foodItem.packing().pack(),foodItem.price());
            System.out.println();
        }
    }
}
