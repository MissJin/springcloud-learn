package com.hcj.springcloud.BuilderPattern;

public class ChickenHamburg extends Hamburg {
    @Override
    public String name() {
        return "ChickenHamburg:鸡肉汉堡";
    }

    @Override
    public float price() {
        return 20.5f;
    }
}
