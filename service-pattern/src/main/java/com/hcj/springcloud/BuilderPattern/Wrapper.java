package com.hcj.springcloud.BuilderPattern;

/**
 * 纸包装
 */
public class Wrapper implements FoodPacking{
    @Override
    public String pack() {
        return "wrapper：纸包装";
    }
}
