package com.hcj.springcloud.BuilderPattern;

/**
 * 瓶子包装
 */
public class Bottle implements FoodPacking{
    @Override
    public String pack() {
        return "Bottle：瓶子包装";
    }
}
