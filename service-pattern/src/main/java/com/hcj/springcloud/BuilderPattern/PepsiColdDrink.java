package com.hcj.springcloud.BuilderPattern;

/**
 * 百事可乐冷饮
 */
public class PepsiColdDrink extends ColdDrink {
    @Override
    public String name() {
        return "PepsiColdDrink:百事可乐冷饮";
    }

    @Override
    public float price() {
        return 6f;
    }
}
