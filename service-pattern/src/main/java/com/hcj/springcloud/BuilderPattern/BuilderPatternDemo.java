package com.hcj.springcloud.BuilderPattern;

public class BuilderPatternDemo {
    public static void main(String[] args) {
        MealBuilder mealBuilder = new MealBuilder();
        Meal vegMeal = mealBuilder.createVegMeal();
        Meal noVegMeal = mealBuilder.createNoVegMeal();
        vegMeal.showItems();
//        foodName:                VegHamburg:蔬菜汉堡 	        foodPacking: wrapper：纸包装 	foodPrice:  10.5
//        foodName:           CokeColdDrink:可口可乐冷饮 	    foodPacking: Bottle：瓶子包装 	foodPrice:   5.0
        noVegMeal.showItems();
//        foodName:            ChickenHamburg:鸡肉汉堡 	        foodPacking: wrapper：纸包装 	foodPrice:  20.5
//        foodName:          PepsiColdDrink:百事可乐冷饮 	    foodPacking: Bottle：瓶子包装 	foodPrice:   6.0
    }
}
