package com.hcj.springcloud.BuilderPattern;

/**
 * 蔬菜汉堡
 */
public class VegHamburg extends Hamburg{
    @Override
    public String name() {
        return "VegHamburg:蔬菜汉堡";
    }

    @Override
    public float price() {
        return 10.5f;
    }
}
