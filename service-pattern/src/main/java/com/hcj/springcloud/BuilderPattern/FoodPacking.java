package com.hcj.springcloud.BuilderPattern;

/**
 * 食物需要何种包装
 */
public interface FoodPacking {
    String pack();
}
