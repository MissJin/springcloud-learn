package com.hcj.springcloud.BuilderPattern;

/**
 * 冷饮类的食物
 */
public abstract class ColdDrink implements FoodItem {

    @Override
    public FoodPacking packing() {
        return new Bottle();
    }

    @Override
    public abstract float price();
}
