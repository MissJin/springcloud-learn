package com.hcj.springcloud.BuilderPattern;

/**
 * 可口可乐冷饮
 */
public class CokeColdDrink extends ColdDrink {
    @Override
    public String name() {
        return "CokeColdDrink:可口可乐冷饮";
    }

    @Override
    public float price() {
        return 5f;
    }
}
