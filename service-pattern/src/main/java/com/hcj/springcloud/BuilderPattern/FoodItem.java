package com.hcj.springcloud.BuilderPattern;

/**
 * 订单中的食物:
 * 例如： 食物名称，需要何种包装
 */
public interface FoodItem {
    /**
     * 食物名称
     */
    String name();

    /**
     * 包装
     */
    FoodPacking packing();

    /**
     * 价格
     */
    float price();
}
