package com.hcj.springcloud.InterceptingFilterPattern;

/**
 * 拦截过滤器设计模式：
 * <br>
 * 当我们想要对应用程序的请求或响应进行一些预处理/后处理时，使用拦截过滤器设计模式。
 * 在将请求传递给实际目标应用程序之前，会在请求上定义和应用过滤器。
 * 过滤器可以执行身份验证/授权/记录或跟踪请求，然后将请求传递给相应的处理程序。
 * <pre>
 * 以下是此类设计模式的实体。
 *      过滤器 - 在请求处理程序执行请求之前或之后执行特定任务的过滤器。
 *      过滤链 - 过滤链带有多个过滤器，有助于在目标上按照定义的顺序执行它们。
 *      目标 - 目标对象是请求处理程序
 *      过滤器管理器 - 过滤器管理器管理过滤器和过滤器链。
 *      客户端 - 客户端是向Target对象发送请求的对象。
 * </pre>
 * <br>---------------概览-----------------<br>
 * 我们将创建一个FilterChain，FilterManager，Target，Client作为代表我们实体的各种对象。
 * AuthenticationFilter和DebugFilter表示具体的过滤器。
 * 我们的演示类InterceptingFilterDemo将使用Client来演示拦截过滤器设计模式。
 */
public class InterceptingFilterDemo {
    public static void main(String[] args) {
        FilterManager filterManager = new FilterManager(new Target());
        filterManager.setFilter(new AuthenticationFilter());
        filterManager.setFilter(new DebugFilter());

        Client client = new Client();
        client.setFilterManager(filterManager);
        client.sendRequest("这是客戶端发出的请求");

        //        FilterManager::filterRequest()-> 这是客戶端发出的请求
        //        AuthenticationFilter::execute(): 这是客戶端发出的请求
        //        DebugFilter::execute(): 这是客戶端发出的请求
        //        Target::execute() -> 这是客戶端发出的请求
    }
}
