package com.hcj.springcloud.InterceptingFilterPattern;

/**
 * 过滤器管理者
 */
public class FilterManager {
    private  FilterChain filterChain;

    public FilterManager(Target target) {
        filterChain = new FilterChain();
        filterChain.setTarget(target);
    }

    public void setFilter(Filter filter){
        filterChain.addFilter(filter);
    }

    public void filterRequest(String request){
        System.out.println("FilterManager::filterRequest()-> "+ request);
        filterChain.execute(request);
    }
}
