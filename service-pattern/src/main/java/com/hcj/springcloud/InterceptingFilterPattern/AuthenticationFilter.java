package com.hcj.springcloud.InterceptingFilterPattern;

public class AuthenticationFilter implements Filter {
    @Override
    public void execute(String request) {
        System.out.println("AuthenticationFilter::execute(): "+ request);
    }
}
