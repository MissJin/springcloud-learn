package com.hcj.springcloud.InterceptingFilterPattern;

public class DebugFilter implements Filter {
    @Override
    public void execute(String request) {
        System.out.println("DebugFilter::execute(): "+ request);
    }
}
