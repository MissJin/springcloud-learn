package com.hcj.springcloud.InterceptingFilterPattern;

/**
 * 接口： 拦截器
 */
public interface Filter {
    void execute(String request);
}
