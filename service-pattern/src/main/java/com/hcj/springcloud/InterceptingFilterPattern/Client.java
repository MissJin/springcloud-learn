package com.hcj.springcloud.InterceptingFilterPattern;

public class Client {
    private FilterManager filterManager;

    public void sendRequest(String request){
        filterManager.filterRequest(request);
    }

    public FilterManager getFilterManager() {
        return filterManager;
    }

    public void setFilterManager(FilterManager filterManager) {
        this.filterManager = filterManager;
    }
}
