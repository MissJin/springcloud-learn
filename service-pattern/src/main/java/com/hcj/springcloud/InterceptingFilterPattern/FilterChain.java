package com.hcj.springcloud.InterceptingFilterPattern;

import java.util.ArrayList;
import java.util.List;

/**
 * 过滤器链
 */
public class FilterChain {
    private List<Filter> list = new ArrayList<>();
    private Target target;

    public void addFilter(Filter filter){
        list.add(filter);
    }

    public void execute(String request){
        for( Filter filter: list){
            filter.execute(request);
        }
        target.execute(request);
    }

    public void setTarget(Target target){
        this.target = target;
    }
}
