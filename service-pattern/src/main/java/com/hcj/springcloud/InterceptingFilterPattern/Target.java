package com.hcj.springcloud.InterceptingFilterPattern;

public class Target {
    public void execute(String request){
        System.out.println("Target::execute() -> "+ request);
    }
}
