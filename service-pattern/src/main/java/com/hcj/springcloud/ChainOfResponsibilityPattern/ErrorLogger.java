package com.hcj.springcloud.ChainOfResponsibilityPattern;

public class ErrorLogger extends AbstractLogger {

    public ErrorLogger(int level) {
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("ErrorLogger::write()\t\t current level:" + this.level + " the message:" + message);
    }
}
