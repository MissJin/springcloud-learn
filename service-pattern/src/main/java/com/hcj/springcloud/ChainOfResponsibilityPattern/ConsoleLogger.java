package com.hcj.springcloud.ChainOfResponsibilityPattern;

public class ConsoleLogger extends AbstractLogger {

    public ConsoleLogger(int level) {
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("ConsoleLogger::write()\t\t current level:" + this.level + " the message:" + message);
    }
}
