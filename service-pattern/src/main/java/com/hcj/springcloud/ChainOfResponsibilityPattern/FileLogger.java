package com.hcj.springcloud.ChainOfResponsibilityPattern;

public class FileLogger extends AbstractLogger {

    public FileLogger(int level) {
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("FileLogger::write()\t\t current level:" + this.level + " the message:" + message);
    }
}
