package com.hcj.springcloud.ChainOfResponsibilityPattern;

/**
 * 责任链模式:
 * <br/>
 * 顾名思义，责任链模式为请求创建了一系列接收者对象。
 * 此模式根据请求类型将请求的发送方和接收方解耦。这种模式属于行为模式。
 * <br/>
 * 在此模式中，通常每个接收器包含对另一个接收器的引用。
 * 如果一个对象无法处理请求，则将其传递给下一个接收器，依此类推。
 */
public class ChainPatternDemo {
    private static AbstractLogger getChainOfLoggers() {
        // 构建一个责任链 3-2-1
        AbstractLogger errorLogger = new ErrorLogger(AbstractLogger.ERROR);
        AbstractLogger fileLogger = new FileLogger(AbstractLogger.DEBUG);
        AbstractLogger consoleLogger = new ConsoleLogger(AbstractLogger.INFO);

        fileLogger.setNextLogger(consoleLogger);
        errorLogger.setNextLogger(fileLogger);
        return errorLogger;
    }

    public static void main(String[] args) {
        AbstractLogger loggerChain = getChainOfLoggers();
        System.out.println("只输出1");
        loggerChain.logMessage(AbstractLogger.INFO, "this is an info level msg");
        System.out.println("先输出等级高的2,再输出1");
        loggerChain.logMessage(AbstractLogger.DEBUG, "this is an debug level msg");
        System.out.println("先输出等级高的3,再2,最后输出1");
        loggerChain.logMessage(AbstractLogger.ERROR, "this is an error level msg");

//        只输出1
//        ConsoleLogger::write()		 current level:1 the message:this is an info level msg
//        先输出等级高的2,再输出1
//        FileLogger::write()		     current level:2 the message:this is an debug level msg
//        ConsoleLogger::write()		 current level:1 the message:this is an debug level msg
//        先输出等级高的3,再2,最后输出1
//        ErrorLogger::write()		     current level:3 the message:this is an error level msg
//        FileLogger::write()		     current level:2 the message:this is an error level msg
//        ConsoleLogger::write()		 current level:1 the message:this is an error level msg
    }


}
