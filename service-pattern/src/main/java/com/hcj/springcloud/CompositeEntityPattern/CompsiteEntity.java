package com.hcj.springcloud.CompositeEntityPattern;


public class CompsiteEntity {
    private CoarseGrainedObject cgo = new CoarseGrainedObject();

    public void setData(String data1, String data2) {
        cgo.setData(data1, data2);
    }

    public String[] getData(){
        return cgo.getData();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
