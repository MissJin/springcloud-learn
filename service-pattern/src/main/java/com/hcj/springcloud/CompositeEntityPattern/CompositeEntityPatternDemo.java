package com.hcj.springcloud.CompositeEntityPattern;

/**
 * 复合实体模式：
 * <br>
 * 复合实体模式用于EJB持久性机制。
 * Composite实体是EJB实体bean，它表示对象图。
 * 更新复合实体时，内部相关对象bean会自动更新为EJB实体bean管理。
 * 以下是Composite Entity Bean的参与者。<pre>
 * 复合实体 - 它是主要的实体bean。它可以是粗粒度的，也可以包含粗粒度的对象以用于持久性目的。
 * 粗粒度对象 - 此对象包含依赖对象。它有自己的生命周期，也管理依赖对象的生命周期。
 * 依赖对象 - 依赖对象是一个对象，它依赖于粗粒度对象的持久性生命周期。
 * 策略 - 策略表示如何实现复合实体。</pre>
 * <br>---------------概览-----------------<br>
 * 我们将创建充当CompositeEntity的CompositeEntity对象。
 * CoarseGrainedObject将是一个包含依赖对象的类。
 * CompositeEntityPatternDemo，我们的演示类将使用Client类来演示Composite Entity模式的使用。
 */
public class CompositeEntityPatternDemo {
    public static void main(String[] args) {
        Client client = new Client();
        client.setData("data1", "data2");
        client.printData();
        client.setData("data3", "data4");
        client.printData();
    }


}
