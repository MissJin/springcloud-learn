package com.hcj.springcloud.CompositeEntityPattern;

public class Client {
    private CompsiteEntity compsiteEntity = new CompsiteEntity();


    public void printData() {
        for (int i = 0; i < compsiteEntity.getData().length; i++) {
            System.out.println("Data: \t"+ compsiteEntity.getData()[i]);
        }
    }


    public void setData(String data1, String data2) {
        compsiteEntity.setData(data1, data2);
    }
}
