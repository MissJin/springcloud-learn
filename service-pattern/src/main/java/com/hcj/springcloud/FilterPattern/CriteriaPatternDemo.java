package com.hcj.springcloud.FilterPattern;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class CriteriaPatternDemo {

    public static void showList(List<Person> list){
        for(Person person: list){
            System.out.println(person);
        }

    }
    public static void main(String[] args) {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person("张三","male","single"));
        personList.add(new Person("老王","male","marry"));
        personList.add(new Person("李丽","female","marry"));
        personList.add(new Person("刘欣","female","single"));

        System.out.println("女--------------");
        Criteria criteriaFemale = new CriteriaFemale();
        List<Person> femaleList = criteriaFemale.meetCriteria(personList);
        showList(femaleList);
        System.out.println("男--------------");
        Criteria criteriaMale = new CriteriaMale();
        List<Person> maleList = criteriaMale.meetCriteria(personList);
        showList(maleList);

        System.out.println("人妖--------------");
        Criteria criteriaAnd = new CriteriaAnd(criteriaFemale, criteriaMale);
        List<Person> femaleAndMale = criteriaAnd.meetCriteria(personList);
        showList(femaleAndMale);

        System.out.println("单身 or females--------------");
        Criteria criteriaOr = new CriteriaOr(new CriteriaSingle(), new CriteriaFemale());
        List<Person> femaleOrSingle = criteriaOr.meetCriteria(personList);
        showList(femaleOrSingle);
    }
}
