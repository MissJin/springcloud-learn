package com.hcj.springcloud.FilterPattern;

import java.util.List;

/**
 * 条件
 */
public interface Criteria {
    List<Person> meetCriteria(List<Person> persons);
}
