package com.hcj.springcloud.FilterPattern;

import java.util.ArrayList;
import java.util.List;

/**
 * 条件：女
 */
public class CriteriaFemale implements Criteria{

    @Override
    public List<Person> meetCriteria(List<Person> persons) {
        List<Person> malePersons = new ArrayList<>();
        for (Person person: persons){
            if(person.getGender().equalsIgnoreCase("female")){
                malePersons.add(person);
            }
        }
        return malePersons;
    }
}
