package com.hcj.springcloud.FilterPattern;

import java.util.ArrayList;
import java.util.List;

/**
 * 条件：未婚
 */
public class CriteriaSingle implements Criteria{

    @Override
    public List<Person> meetCriteria(List<Person> persons) {
        List<Person> malePersons = new ArrayList<>();
        for (Person person: persons){
            if(person.getMaritalStatus().equalsIgnoreCase("single")){
                malePersons.add(person);
            }
        }
        return malePersons;
    }
}
