package com.hcj.springcloud.FactoryPattern;

/**
 * step: 1
 */
public interface Shape {
    void draw();
}
