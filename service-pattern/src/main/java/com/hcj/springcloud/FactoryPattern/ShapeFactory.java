package com.hcj.springcloud.FactoryPattern;

public class ShapeFactory {
    public Shape getShape(String shapeType){
        /**
         * break 是直接退出switch语句
         * return 是退出该函数 也就是switch语句块后面的语句也不执行了
         */
        switch (shapeType.toLowerCase()){
            case "circle":
                return new Circle();
            case "rectangle":
                return new Rectangle();
            case "square":
                return new Square();
            default:
                return null;
        }
    }

    public static void main(String[] args) {
        ShapeFactory shapeFactory = new ShapeFactory();
        Shape circle = shapeFactory.getShape("circle");
        Shape rectangle = shapeFactory.getShape("rectangle");
        Shape square = shapeFactory.getShape("square");

        circle.draw();
        rectangle.draw();
        square.draw();
    }
}
