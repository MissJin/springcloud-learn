package com.hcj.springcloud.StatePattern;

public class StartState implements State {

    @Override
    public void doAction(Context context) {
        System.out.println("StartState::doAction() 开始");
        context.setState(this);
    }

    @Override
    public String toString() {
        return "Start State";
    }
}
