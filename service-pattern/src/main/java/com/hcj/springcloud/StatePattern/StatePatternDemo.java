package com.hcj.springcloud.StatePattern;
/**
 * 状态模式：
 * <br>
 * 在状态模式中，类行为会根据其状态而更改。
 * 这种类型的设计模式属于行为模式。
 * 在状态模式中，我们创建表示各种状态的对象和上下文对象，其行为随着状态对象的变化而变化。
 * <br>---------------概览-----------------<br>
 * 我们将创建一个定义动作的State接口和实现State接口的具体状态类。
 * 上下文是一个携带状态机的类。
 * StatePatternDemo是我们的演示类，它将使用Context和状态对象来根据它所处的状态类型演示Context行为的变化。
 */
public class StatePatternDemo {
    public static void main(String[] args) {
        Context context = new Context();
        StartState startState = new StartState();
        startState.doAction(context);

        System.out.println(context.getState().toString());

        StopState stopState = new StopState();
        stopState.doAction(context);

        System.out.println(context.getState().toString());
        //        StartState::doAction() 开始
        //        Start State
        //        StopState::doAction() 暂停
        //        Stop State

    }
}
