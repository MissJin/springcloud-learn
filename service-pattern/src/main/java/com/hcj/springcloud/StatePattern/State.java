package com.hcj.springcloud.StatePattern;

public interface State {
    void doAction(Context context);
}
