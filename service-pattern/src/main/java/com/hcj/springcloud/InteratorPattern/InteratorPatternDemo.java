package com.hcj.springcloud.InteratorPattern;

/**
 * 迭代器模式:
 * <br>
 * 是Java和.Net编程环境中非常常用的设计模式。
 * 此模式用于获取以顺序方式访问集合对象元素的方法，而无需了解其基础表示。
 * 迭代器模式属于行为模式类别。
 * <br>---------------概览-----------------<br>
 * 我们将创建一个迭代导航方法的Iterator接口和一个重构迭代器的Container接口。
 * 实现Container接口的具体类将负责实现Iterator接口并使用它
 * IteratorPatternDemo，我们的演示类将使用NamesRepository，一个具体的类实现打印的名称存储为一个集合NamesRepository
 */
public class InteratorPatternDemo {
    public static void main(String[] args) {
        NameRepository nameRepository = new NameRepository();
        Iterator iterator = nameRepository.getIterator();
        while (iterator.hasNext()){
            String name = (String) iterator.next();
            System.out.println("name is:"+name);
        }
    }
}
