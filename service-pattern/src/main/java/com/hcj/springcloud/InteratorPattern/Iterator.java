package com.hcj.springcloud.InteratorPattern;

public interface Iterator {
    boolean hasNext();
    Object next();
}
