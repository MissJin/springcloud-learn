package com.hcj.springcloud.MediatorPattern;

import java.time.LocalDateTime;

public class ChatRoom {
    public static void showMessage(User user, String message) {
        System.out.println(LocalDateTime.now() + "\t[" + user.getName()
                + "]:\t" + message);
    }
}
