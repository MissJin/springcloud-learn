package com.hcj.springcloud.MediatorPattern;

/**
 * 中介模式：
 * 中介模式用于减少多个对象或类之间的通信复杂性。
 * 此模式提供了一个中介类，它通常处理不同类之间的所有通信，并支持通过松散耦合轻松维护代码。
 * Mediator模式属于行为模式类别。
 * <br>---------------概览-----------------<br>
 * 我们通过聊天室的示例演示中介模式，其中多个用户可以向聊天室发送消息，并且聊天室负责向所有用户显示消息。
 * 我们创建了两个类ChatRoom和User。
 * 用户对象将使用ChatRoom方法来共享其消息。
 * 我们的演示类MediatorPatternDemo将使用User对象来显示它们之间的通信。
 */
public class MediatorPatternDemo {
    public static void main(String[] args) {
        User hcj = new User("hcj");
        User lff = new User("lff");
        User yjj = new User("yjj");

        hcj.sendMessage("hello everybody!");
        lff.sendMessage("hello hcj!");
        yjj.sendMessage("hello !");
    }
}
