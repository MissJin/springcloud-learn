package com.hcj.springcloud.BusinessDelegatePattern;

public class BusinessDelegate {
    private BusinessLookup lookup = new BusinessLookup();
    private BusinessService service;
    private String serviceType;

    public void doTask(){
        System.out.println("BusinessDelegate::doTask()");
        service = lookup.getBusinessService(serviceType);
        service.doProcessing();
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }
}
