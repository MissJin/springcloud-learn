package com.hcj.springcloud.BusinessDelegatePattern;

public class JMSService implements BusinessService {
    @Override
    public void doProcessing() {
        System.out.println("JMSService::doProcessing()");
    }
}
