package com.hcj.springcloud.BusinessDelegatePattern;

/**
 * 接口： 商业服务
 */
public interface BusinessService {
    void doProcessing();
}
