package com.hcj.springcloud.BusinessDelegatePattern;

public class Client {
    private BusinessDelegate service;

    public Client(BusinessDelegate service) {
        this.service = service;
    }

    public void doTask(){
        System.out.println("Client::doProcessing()");
        service.doTask();
    }
}
