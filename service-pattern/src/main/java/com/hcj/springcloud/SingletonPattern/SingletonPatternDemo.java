package com.hcj.springcloud.SingletonPattern;

public class SingletonPatternDemo {
    public static void main(String[] args) {
        SingleObject instance = SingleObject.getInstance();
        instance.function();
        // SingleObject instanceByNew = new SingleObject(); // 不允许通过new 关键字来初始化对象
        SingleObject instance1 = SingleObject.getInstance();
        if(instance == instance1){
            System.out.println("instance == instance1");
        }else {
            System.out.println("instance != instance1");
        }
    }
}
