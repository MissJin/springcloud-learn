package com.hcj.springcloud.SingletonPattern;

/**
 * 单例对象
 */
public class SingleObject {
    private static SingleObject instance = new SingleObject();
    private SingleObject(){}

    public static SingleObject getInstance(){
        return instance;
    }

    public void function(){
        System.out.println("SingleObject:: function()");
    }

}
