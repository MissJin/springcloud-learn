package com.hcj.springcloud.ObserverPattern;

public class BinaryObserver extends Observer {
    public BinaryObserver(Subject subject) {
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println("二进制：BinaryObserver::update() \t state:"+ Integer.toBinaryString(subject.getState()));
    }
}
