package com.hcj.springcloud.ObserverPattern;

/**
 * 抽象类： 观察者
 */
public abstract class Observer {
    protected Subject subject;
    public abstract void update();
}
