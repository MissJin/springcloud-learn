package com.hcj.springcloud.ObserverPattern;

/**
 * 观察者模式：
 * <br>
 * 当对象之间存在一对多关系时使用观察者模式，例如，如果修改了一个对象，则会自动通知其依赖对象。
 * 观察者模式属于行为模式类别
 * <br>---------------概览-----------------<br>
 * 观察者模式使用三个actor类。主题，观察者和客户。
 * Subject是具有将观察者附加和分离到客户端对象的方法的对象。
 * 我们创建了一个抽象类Observer和一个扩展类Observer的具体类Subject。
 * ObserverPatternDemo，我们的演示类，将使用Subject和具体的类对象来显示操作中的观察者模式。
 */
public class ObserverPatternDemo {
    public static void main(String[] args) {
        Subject subject = new Subject();
        HexaObserver hexaObserver = new HexaObserver(subject);
        OctalObserver octalObserver = new OctalObserver(subject);
        BinaryObserver binaryObserver = new BinaryObserver(subject);

        System.out.println("first state change:15");
        subject.setState(15);
        System.out.println("second state change: 10");
        subject.setState(10);
        System.out.println("被观察者subject，主动移除观察者:binaryObserver");
        subject.notAttach(binaryObserver);
        System.out.println("third state change: 8");
        subject.setState(8);
        //        first state change:15
        //        十六进制：HexaObserver::update() 	 state:f
        //        八进制：OctalObserver::update() 	 state:17
        //        二进制：BinaryObserver::update() 	 state:1111
        //        second state change: 10
        //        十六进制：HexaObserver::update() 	 state:a
        //        八进制：OctalObserver::update() 	 state:12
        //        二进制：BinaryObserver::update() 	 state:1010
        //        被观察者，主动移除观察者:binaryObserver
        //        third state change: 8
        //        十六进制：HexaObserver::update() 	 state:8
        //        八进制：OctalObserver::update() 	 state:10
    }
}
