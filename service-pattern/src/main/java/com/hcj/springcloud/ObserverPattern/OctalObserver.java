package com.hcj.springcloud.ObserverPattern;

public class OctalObserver extends Observer {
    public OctalObserver(Subject subject) {
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println("八进制：OctalObserver::update() \t state:"+ Integer.toOctalString(subject.getState()));
    }
}
