package com.hcj.springcloud.ObserverPattern;

public class HexaObserver extends Observer {
    public HexaObserver(Subject subject) {
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println("十六进制：HexaObserver::update() \t state:"+ Integer.toHexString(subject.getState()));
    }
}
