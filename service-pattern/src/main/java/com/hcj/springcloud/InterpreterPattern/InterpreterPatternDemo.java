package com.hcj.springcloud.InterpreterPattern;

/**
 * 解释器模式：
 * <br>
 * 提供了一种评估语言语法或表达的方法。这种模式属于行为模式。
 * 此模式涉及实现表达式接口，该接口用于解释特定上下文。
 * 此模式用于SQL解析，符号处理引擎等。
 * <br>---------------概览-----------------<br>
 * 我们将创建一个接口 Expression 和实现Expression接口的具体类。
 * 定义了一个类TerminalExpression，它充当相关上下文的主要解释器。
 * 其他类OrExpression，AndExpression用于创建组合表达式。
 * InterpreterPatternDemo是我们的演示类，它将使用Expression类来创建规则并演示表达式的解析。
 */
public class InterpreterPatternDemo {

    // Robert 和 John 都是male
    public static Expression getMaleExpression(){
        Expression robert = new TerminalExpression("Robert");
        Expression john = new TerminalExpression("John");
        return new OrExpression(robert, john);
    }

    // Jack is amarried woman
    public static Expression getMarriedWomanExpression(){
        Expression jack_one = new TerminalExpression("Jack");
        Expression married = new TerminalExpression("is a married woman");
        return new AndExpression(jack_one, married);
    }

    public static void main(String[] args) {
        Expression isMale = getMaleExpression();
        Expression isMarriedWoman = getMarriedWomanExpression();

        System.out.println("John is a male?");
        System.out.println(isMale.interpret("John"));

        System.out.println("lisa is a male?");
        System.out.println(isMale.interpret("lisa"));

        System.out.println("Jack is a married woman?");
        System.out.println(isMarriedWoman.interpret("Jack is a married woman"));

        //        John is a male?
        //        true
        //        lisa is a male?
        //        false
        //        Jack is a married woman?
        //        true
    }


}
