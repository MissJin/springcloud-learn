package com.hcj.springcloud.InterpreterPattern;

/**
 * 解释接口
 */
public interface Expression {
    boolean interpret(String context);
}
