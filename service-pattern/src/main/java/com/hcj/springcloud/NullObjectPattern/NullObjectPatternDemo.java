package com.hcj.springcloud.NullObjectPattern;

/**
 * 空对象模式：
 * <br>
 * 在Null Object模式中，null对象替换NULL对象实例的检查。
 * Null Object反映了无关系，而不是检查是否为空值。
 * 在数据不可用的情况下，此类Null对象还可用于提供默认行为。
 * 在Null对象模式中，我们创建一个抽象类，
 * 指定要完成的各种操作，
 * 扩展此类的具体类和一个null对象类，
 * 提供此类的任何实现，并且将在我们需要检查空值的地方无缝使用。
 * <br>---------------概览-----------------<br>
 * 我们将创建一个定义操作的AbstractCustomer抽象类。
 * 这里是客户的名称和扩展AbstractCustomer类的具体类。
 * 创建工厂类CustomerFactory，以根据传递给它的客户名称返回RealCustomer或NullCustomer对象。
 * 我们的演示类NullPatternDemo将使用CustomerFactory来演示Null Object模式的使用。
 */
public class NullObjectPatternDemo {
    public static void main(String[] args) {
        AbstractCustomer missjin = CustomerFactory.getCustomer("missjin");
        AbstractCustomer hcj = CustomerFactory.getCustomer("hcj");
        AbstractCustomer lff = CustomerFactory.getCustomer("lff");
        AbstractCustomer xxx = CustomerFactory.getCustomer("xxx");
        AbstractCustomer yyy = CustomerFactory.getCustomer("yyy");

        System.out.println("customers is:");
        System.out.println(missjin.getName());
        System.out.println(xxx.getName());
        System.out.println(hcj.getName());
        System.out.println(yyy.getName());
        System.out.println(lff.getName());
        //        customers is:
        //        missjin
        //        [xxx] not available in customer database.
        //        hcj
        //        [yyy] not available in customer database.
        //        lff
    }
}
