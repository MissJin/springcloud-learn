package com.hcj.springcloud.NullObjectPattern;

public class NullCustomer extends AbstractCustomer {
    public NullCustomer(String name) {
        this.name = name;
    }

    @Override
    public boolean isNull() {
        return true;
    }

    @Override
    public String getName() {
        return "["+ this.name + "] not available in customer database.";
    }
}
