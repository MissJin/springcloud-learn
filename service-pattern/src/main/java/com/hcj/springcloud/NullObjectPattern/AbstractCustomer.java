package com.hcj.springcloud.NullObjectPattern;

/**
 * 抽象类： 顾客
 */
public abstract class AbstractCustomer {
    protected String name;
    public abstract boolean isNull();
    public abstract String getName();
}
