package com.hcj.springcloud.CommandPattern;

public interface Order {
    void execute();
}
