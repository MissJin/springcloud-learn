package com.hcj.springcloud.CommandPattern;

/**
 * 请求
 */
public class Stock {
    private String name = "ABC";
    private int quantity = 10;

    public void buy() {
        System.out.println("Stock:: buy() \tname:" + name + "\tquantity:" + quantity);
    }

    public void sell() {
        System.out.println("Stock:: sell() \tname:" + name + "\tquantity:" + quantity);
    }
}
