package com.hcj.springcloud.CommandPattern;

/**
 * 命令模式:
 * <br>
 * 是一种数据驱动的设计模式，属于行为模式类别。
 * 请求作为命令包装在对象下并传递给调用者对象。
 * Invoker对象查找可以处理此命令的相应对象，并将该命令传递给执行该命令的相应对象。
 * <br>---------------概览-----------------<br>
 * 我们创建了一个作为命令的接口Order。
 * 我们创建了一个充当请求的Stock类。
 * 我们有具体的命令类BuyStock和SellStock实现Order接口，它将进行实际的命令处理。
 * 创建一个类Broker（经纪人），它充当调用者对象。它可以接受和下订单。
 * Broker对象使用命令模式来识别哪个对象将根据命令的类型执行哪个命令。
 * 我们的演示类CommandPatternDemo将使用Broker类来演示命令模式
 */
public class CommandPatternDemo {
    public static void main(String[] args) {
        Stock abcStock = new Stock();
        BuyStock buyStock = new BuyStock(abcStock);
        BuyStock buyStock1 = new BuyStock(new Stock());
        SellStock sellStock = new SellStock(abcStock);

        Broker broker = new Broker();
        broker.takeOrder(buyStock);
        broker.takeOrder(buyStock1);
        broker.takeOrder(sellStock);

        // 经纪人：执行命令
        broker.placeOrders();
    }
}
