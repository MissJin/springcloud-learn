package com.hcj.springcloud.CommandPattern;

public class BuyStock implements Order {
    private Stock abcStock;

    @Override
    public void execute() {
        abcStock.buy();
    }

    public BuyStock(Stock abcStock) {
        this.abcStock = abcStock;
    }
}
