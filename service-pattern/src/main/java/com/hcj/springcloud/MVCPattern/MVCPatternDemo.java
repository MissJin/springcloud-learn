package com.hcj.springcloud.MVCPattern;

/**
 * MVC模式：
 * <br>
 * MVC Pattern代表:模型 - 视图 - 控制器模式。此模式用于分离应用程序的问题。
 * 模型 - 模型表示携带数据的对象或JAVA POJO。
 * 如果控制器的数据发生变化，它也可以有更新控制器的逻辑
 * 视图 - 视图表示模型包含的数据的可视化。
 * 控制器 - 控制器作用于模型和视图。它控制数据流到模型对象，并在数据发生变化时更新视图。
 * 它使视图和模型分开。
 * <br>---------------概览-----------------<br>
 * 我们将创建一个充当模型的Student对象。
 * StudentView将是一个视图类，可以在控制台上打印学生详细信息，
 * StudentController是负责在Student对象中存储数据的控制器类，
 * 并相应地更新视图StudentView。
 * 我们的演示类MVCPatternDemo将使用StudentController来演示MVC模式的使用。
 */
public class MVCPatternDemo {
    public static void main(String[] args) {
        Student model = retriveStudentFromDatabase();

        StudentView view = new StudentView();

        StudentController controller = new StudentController(model, view);

        controller.updateView();

        controller.setStudentName("Name-X");

        controller.updateView();
        //        Student{rollNo='No9', name='Name9'}
        //        Student{rollNo='No9', name='Name-X'}
    }

    private static Student retriveStudentFromDatabase(){
        Student student = new Student();
        int num = (int)(Math.random() * 10);
        student.setRollNo("No"+ num);
        student.setName("Name"+ num);
        return student;
    }
}
