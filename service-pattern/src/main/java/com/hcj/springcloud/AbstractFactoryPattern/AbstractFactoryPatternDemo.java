package com.hcj.springcloud.AbstractFactoryPattern;

public class AbstractFactoryPatternDemo {

    public static void main(String[] args) {
        AbstractFactory factory = FactoryProducer.getFactory(false);
        Shape circle = factory.getShape("circle");
        if(circle == null){
            System.err.println("此工厂不产circle");
        }else {
            circle.draw();
        }

        Shape rectangle = factory.getShape("rectangle");
        if(rectangle == null){
            System.err.println("此工厂不产rectangle");
        }else {
            rectangle.draw();
        }

    }
}
