package com.hcj.springcloud.AbstractFactoryPattern;


/**
 * step: 2.1
 */
public class Rectangle implements Shape {
    @Override
    public void draw() {
        System.out.println("Rectangel :: draw() method.");
    }
}
