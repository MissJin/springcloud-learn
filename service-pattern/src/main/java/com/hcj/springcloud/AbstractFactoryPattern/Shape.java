package com.hcj.springcloud.AbstractFactoryPattern;

/**
 * step: 1
 */
public interface Shape {
    void draw();
}
