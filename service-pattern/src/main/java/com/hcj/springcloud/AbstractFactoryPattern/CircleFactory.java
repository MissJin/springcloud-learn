package com.hcj.springcloud.AbstractFactoryPattern;

/**
 * 只负责生产圆的形状工厂
 */
public class CircleFactory extends  AbstractFactory {
    @Override
    public Shape getShape(String shapeType) {
        switch (shapeType.toLowerCase()){
            case "circle":
                return new Circle();
            default:
                return null;
        }
    }
}
