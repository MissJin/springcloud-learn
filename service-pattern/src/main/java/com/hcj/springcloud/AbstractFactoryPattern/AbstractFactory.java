package com.hcj.springcloud.AbstractFactoryPattern;

/**
 * 虚拟工厂
 */
public abstract class AbstractFactory {
    abstract Shape getShape(String shapeType);
}
