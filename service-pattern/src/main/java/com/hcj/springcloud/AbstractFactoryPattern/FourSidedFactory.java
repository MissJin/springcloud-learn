package com.hcj.springcloud.AbstractFactoryPattern;

/**
 * 只负责生产4边行的形状工厂
 */
public class FourSidedFactory extends  AbstractFactory {
    @Override
    public Shape getShape(String shapeType) {
        switch (shapeType.toLowerCase()){
            case "rectangle":
                return new Rectangle();
            case "square":
                return new Square();
            default:
                return null;
        }
    }
}
