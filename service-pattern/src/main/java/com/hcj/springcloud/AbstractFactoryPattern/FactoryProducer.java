package com.hcj.springcloud.AbstractFactoryPattern;

/**
 * 工厂创建器：
 * 负责创建工厂
 * @des 工厂是按需构建
 */
public class FactoryProducer {
    public static AbstractFactory getFactory(boolean rounded){
        if(rounded){
            return new CircleFactory();
        }else {
            return new FourSidedFactory();
        }
    }

}
