package com.hcj.springcloud.ServiceLocatorPattern;

/**
 * 接口： 服务
 */
public interface Service {
    String getName();
    void execute();
}
