package com.hcj.springcloud.ServiceLocatorPattern;

public class ServiceOne implements Service {
    @Override
    public String getName() {
        System.out.println("ServiceOne::getName()");
        return "serviceOne";
    }

    @Override
    public void execute() {
        System.out.println("ServiceOne::execute()");
    }
}
