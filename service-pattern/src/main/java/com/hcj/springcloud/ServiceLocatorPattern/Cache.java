package com.hcj.springcloud.ServiceLocatorPattern;

import java.util.ArrayList;
import java.util.List;

/**
 * 服务缓存类
 */
public class Cache {
    private List<Service> services;

    public Cache() {
        services = new ArrayList<>();
    }

    public Service getService(String serviceName){
        for (Service service: services){
            if(service.getName().equalsIgnoreCase(serviceName)){
                System.out.println("returning cached service " + serviceName +" object");
                return service;
            }
        }
        return null;
    }

    public void addService(Service newService){
        Boolean exists = false;
        for (Service service : services){
            if(service.getName().equalsIgnoreCase(newService.getName())){
                exists = true;
            }
        }
        if (!exists){
            services.add(newService);
        }
    }
}
