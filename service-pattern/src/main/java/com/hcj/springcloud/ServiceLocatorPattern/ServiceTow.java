package com.hcj.springcloud.ServiceLocatorPattern;

public class ServiceTow implements Service {
    @Override
    public String getName() {
        System.out.println("ServiceTow::getName()");
        return "serviceTow";
    }

    @Override
    public void execute() {
        System.out.println("ServiceTow::execute()");
    }
}
