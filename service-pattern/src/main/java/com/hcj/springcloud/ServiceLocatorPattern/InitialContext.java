package com.hcj.springcloud.ServiceLocatorPattern;

public class InitialContext {
   public Object lookup(String jndiName){
        System.out.println("InitialContext::lookup()");

        if (jndiName.equalsIgnoreCase("serviceOne")){
            System.out.println("looking up and createing a new serviceOne object");
            return new ServiceOne();
        }else if (jndiName.equalsIgnoreCase("serviceTow")){
            System.out.println("looking up and createing a new serviceTow object");
            return new ServiceTow();
        }
        return null;
    }
}
