package com.hcj.springcloud.ServiceLocatorPattern;

/**
 * 服务定位器设计模式：
 * <br>
 * 当我们想要使用JNDI查找定位各种服务时，使用服务定位器设计模式。
 * 考虑到为服务查找JNDI的高成本，Service Locator模式使用缓存技术。
 * 第一次需要服务时，Service Locator在JNDI中查找并缓存服务对象。
 * 通过Service Locator进行进一步查找或相同的服务在其缓存中完成，这在很大程度上提高了应用程序的性能。
 * 以下是此类设计模式的实体。
 * <pre>
 * 1.服务 - 将处理请求的实际服务。在JNDI服务器中查看此类服务的引用。
 * 2.上下文/初始上下文 - JNDI上下文包含用于查找目的的服务的引用。
 * 3.服务定位器 - 服务定位器是通过JNDI查找缓存服务来获取服务的单一联系点。
 * 4.缓存 - 缓存以存储服务的引用以重用它们
 * 5.客户端 - 客户端是通过ServiceLocator调用服务的对象。
 * </pre>
 * <br>---------------概览-----------------<br>
 * 我们将创建一个ServiceLocator，InitialContext，Cache，Service作为代表我们实体的各种对象。
 * Service1和Service2代表具体服务。
 * ServiceLocatorPatternDemo，我们的演示类，
 * 在这里充当客户端，并将使用ServiceLocator来演示服务定位器设计模式。
 */
public class ServiceLocatorPatternDemo {
    public static void main(String[] args) {
        System.err.println("[1] serviceOne");
        Service service = ServiceLocator.getService("serviceOne");
        service.execute();


        System.err.println("[2] serviceTow");
        service = ServiceLocator.getService("serviceTow");
        service.execute();


        System.err.println("[3] serviceOne");
        service = ServiceLocator.getService("serviceOne");
        service.execute();

        System.err.println("[4] serviceTow");
        service = ServiceLocator.getService("serviceTow");
        service.execute();


        //        [1] serviceOne
        //        ServiceLocator->cache在静态代码块中初始化
        //        InitialContext::lookup()
        //        looking up and createing a new serviceOne object
        //        ServiceOne::execute()
        //        [2] serviceTow
        //        ServiceOne::getName()
        //        InitialContext::lookup()
        //        looking up and createing a new serviceTow object
        //        ServiceOne::getName()
        //        ServiceTow::getName()
        //        ServiceTow::execute()
        //        [3] serviceOne
        //        ServiceOne::getName()
        //        returning cached service serviceOne object
        //        ServiceOne::execute()
        //        [4] serviceTow
        //        ServiceOne::getName()
        //        ServiceTow::getName()
        //        returning cached service serviceTow object
        //        ServiceTow::execute()

    }
}
