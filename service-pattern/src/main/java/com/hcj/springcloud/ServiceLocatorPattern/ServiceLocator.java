package com.hcj.springcloud.ServiceLocatorPattern;

public class ServiceLocator {
    private static Cache cache;

    static {
        System.out.println("ServiceLocator->cache在静态代码块中初始化");
        cache = new Cache();
    }
    public static Service getService(String jndiName){
        Service service = cache.getService(jndiName);
        if (service != null){
            return service;
        }
        InitialContext context = new InitialContext();
        Service serviceFromContext = (Service) context.lookup(jndiName);
        cache.addService(serviceFromContext);
        return serviceFromContext;
    }

}
