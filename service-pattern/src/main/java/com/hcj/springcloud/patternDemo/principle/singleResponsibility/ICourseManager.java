package com.hcj.springcloud.patternDemo.principle.singleResponsibility;

/**
 * date:2019/7/5
 * <p>
 * 课程管理接口
 */
public interface ICourseManager {

    void studyCourse();

    void refundCourse();
}
