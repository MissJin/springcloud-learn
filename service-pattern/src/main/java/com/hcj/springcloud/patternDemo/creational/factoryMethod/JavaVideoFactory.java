package com.hcj.springcloud.patternDemo.creational.factoryMethod;

/**
 *
 */
public class JavaVideoFactory extends VideoFactory {
    @Override
    public Video getVideo() {
        return new JavaVideo();
    }
}
