package com.hcj.springcloud.patternDemo.structural.facade;

import lombok.Data;

/**
 * date:2019/7/15
 * 积分礼物
 */

@Data
public class PointsGift {

    /**
     * name
     */
    private String name;

    public PointsGift(String name) {
        this.name = name;
    }
}
