package com.hcj.springcloud.patternDemo.structural.adapter.ACToDC;

/**
 * date:2019/7/15
 * 5V的直流电
 */
public interface DC5 {
    int outputDC5();
}

