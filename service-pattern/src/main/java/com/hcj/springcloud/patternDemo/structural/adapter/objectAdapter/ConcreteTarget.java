package com.hcj.springcloud.patternDemo.structural.adapter.objectAdapter;


/**
 * date:2019/7/15
 */
public class ConcreteTarget implements Target {

    @Override
    public void request() {
        System.out.println("concrete target");
    }
}
