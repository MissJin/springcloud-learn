package com.hcj.springcloud.patternDemo.structural.adapter.ACToDC;

/**
 * date:2019/7/15
 * 220V 交流电
 */
public class AC220 {

    public int inputAC() {
        int inputAC = 220;
        System.out.println("输入220V 交流电");
        return inputAC;
    }
}
