package com.hcj.springcloud.patternDemo.principle.interfaceSegregation;

/**
 * date:2019/7/8
 */
public interface ISwimInterface {
    void swim();
}
