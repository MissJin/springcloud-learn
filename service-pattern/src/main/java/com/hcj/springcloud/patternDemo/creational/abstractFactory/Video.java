package com.hcj.springcloud.patternDemo.creational.abstractFactory;

/**
 *
 */
public abstract class Video {

    abstract void product();

}
