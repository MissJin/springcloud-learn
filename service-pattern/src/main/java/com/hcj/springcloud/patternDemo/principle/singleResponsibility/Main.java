package com.hcj.springcloud.patternDemo.principle.singleResponsibility;

/**
 * date:2019/7/5
 */

public class Main {
    public static void main(String[] args) {
        new Student(new CourseImpl());
    }
}
