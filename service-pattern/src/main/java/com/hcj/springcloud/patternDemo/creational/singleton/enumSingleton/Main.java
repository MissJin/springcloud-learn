package com.hcj.springcloud.patternDemo.creational.singleton.enumSingleton;

/**
 * date:2019/7/12
 */
public class Main {

    public static void main(String[] args) {
        EnumSingleton instance = EnumSingleton.getInstance();
        instance.testPrint();
    }
}
