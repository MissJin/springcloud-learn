package com.hcj.springcloud.patternDemo.structural.flyweight;

/**
 * date:2019/7/16
 */
public interface Employee {

    void report();

}
