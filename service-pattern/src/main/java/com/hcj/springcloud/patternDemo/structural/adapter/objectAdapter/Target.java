package com.hcj.springcloud.patternDemo.structural.adapter.objectAdapter;

/**
 * date:2019/7/15
 */
public interface Target {

    void request();
}
