package com.hcj.springcloud.patternDemo.structural.adapter.objectAdapter;

/**
 * date:2019/7/15
 */
public class Adaptee {

    public void adapteeRequest() {
        System.out.println("组合 被适配者的方法");
    }
}
