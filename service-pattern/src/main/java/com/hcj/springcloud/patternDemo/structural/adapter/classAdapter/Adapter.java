package com.hcj.springcloud.patternDemo.structural.adapter.classAdapter;

/**
  *
 * date:2019/7/15
 * 类适配器
 */
public class Adapter extends Adaptee implements Target {

    @Override
    public void request() {
        super.adapteeRequest();
    }
}
