package com.hcj.springcloud.patternDemo.structural.proxy;

/**
 * date:2019/7/22
 */
public interface IOrderDao {
    int insert(Order order);
}
