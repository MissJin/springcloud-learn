package com.hcj.springcloud.patternDemo.structural.proxy;

/**
 * date:2019/7/22
 */
public interface IOrderService {

    int saveOrder(Order order);

    int delete(Order order);
}
