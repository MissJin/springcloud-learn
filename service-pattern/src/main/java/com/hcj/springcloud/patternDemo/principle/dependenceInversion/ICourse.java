package com.hcj.springcloud.patternDemo.principle.dependenceInversion;

/**
 * date:2019/7/5
 */
public interface ICourse {

    void studyCourse();

}
