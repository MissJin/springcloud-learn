package com.hcj.springcloud.patternDemo.principle.dependenceInversion;

/**
  *
 * date:2019/7/5
 */
public class Main {

    public static void main(String[] args) {
        Student student = new Student();
        student.studentStudyCourse(new JavaCourse());
        student.studentStudyCourse(new PythonCourse());
    }
}
