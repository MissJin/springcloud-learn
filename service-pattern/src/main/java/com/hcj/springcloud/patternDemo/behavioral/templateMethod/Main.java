package com.hcj.springcloud.patternDemo.behavioral.templateMethod;

/**
 * date:2019/7/22
 * 模板方法模式测试
 */
public class Main {

    public static void main(String[] args) {
        DesignPatternCourse designPatternCourse = new DesignPatternCourse(true);
        designPatternCourse.makeCourse();

        PECourse peCourse = new PECourse();
        peCourse.makeCourse();
    }
}
