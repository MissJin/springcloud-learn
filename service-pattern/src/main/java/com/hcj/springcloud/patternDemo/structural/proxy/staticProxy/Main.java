package com.hcj.springcloud.patternDemo.structural.proxy.staticProxy;


import com.hcj.springcloud.patternDemo.structural.proxy.Order;

/**
 * date:2019/7/22
 * 静态代理测试
 */
public class Main {

    public static void main(String[] args) {
        Order order = new Order();
        order.setUserId(1);

        OrderServiceStaticProxy staticProxy = new OrderServiceStaticProxy();
        staticProxy.saveOrder(order);
    }
}
