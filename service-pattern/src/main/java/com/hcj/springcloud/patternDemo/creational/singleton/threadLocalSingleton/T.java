package com.hcj.springcloud.patternDemo.creational.singleton.threadLocalSingleton;

/**
 * date:2019/7/12
 */
public class T implements Runnable {
    @Override
    public void run() {
        ThreadLocalSingleton instance = ThreadLocalSingleton.getInstance();
        System.err.println(instance);
    }
}
