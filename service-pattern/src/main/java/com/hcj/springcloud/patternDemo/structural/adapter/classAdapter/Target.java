package com.hcj.springcloud.patternDemo.structural.adapter.classAdapter;

/**
 * date:2019/7/15
 */
public interface Target {

    public void request();
}
