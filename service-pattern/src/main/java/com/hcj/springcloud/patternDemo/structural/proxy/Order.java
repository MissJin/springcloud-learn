package com.hcj.springcloud.patternDemo.structural.proxy;

import lombok.Data;

/**
 * date:2019/7/22
 */

@Data
public class Order {

    private Object orderInfo;

    /**
     * userId
     */
    private Integer userId;


}
