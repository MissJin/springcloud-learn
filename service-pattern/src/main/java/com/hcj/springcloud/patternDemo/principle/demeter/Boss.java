package com.hcj.springcloud.patternDemo.principle.demeter;

/**
 *
 */
public class Boss {

    public void commandCheckNumber() {
        new TeamLeader().checkNumberOfCourses();
    }
}
