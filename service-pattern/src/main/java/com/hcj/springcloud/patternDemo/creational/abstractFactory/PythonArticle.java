package com.hcj.springcloud.patternDemo.creational.abstractFactory;

/**
 *
 */
public class PythonArticle extends Article {
    @Override
    public void product() {
        System.out.println("编写Python手记");
    }
}
