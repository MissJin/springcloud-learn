package com.hcj.springcloud.patternDemo.principle.singleResponsibility;

/**
 * date:2019/7/5
 * 增加课程积分接口
 */
public interface ICourseIntegral {
    void exchangeIntegral();
}
