package com.hcj.springcloud.patternDemo.creational.abstractFactory;

/**
 *
 */
public abstract class CourseFactory {

    abstract Video getVideo();

    abstract Article getArticle();
}
