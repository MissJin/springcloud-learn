package com.hcj.springcloud.patternDemo.structural.bridge;

/**
 * date:2019/7/16
 */
public interface Account {

    Account openAccount();

    void showAccountType();

}
