package com.hcj.springcloud.patternDemo.structural.adapter.classAdapter;

/**
 * date:2019/7/15
 */
public class Adaptee {

    public void adapteeRequest() {
        System.out.println("被适配者的方法");
    }
}
