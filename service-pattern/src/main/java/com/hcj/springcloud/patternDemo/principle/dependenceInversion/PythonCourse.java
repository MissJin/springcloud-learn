package com.hcj.springcloud.patternDemo.principle.dependenceInversion;

/**
 * date:2019/7/5
 */
public class PythonCourse implements ICourse {
    @Override
    public void studyCourse() {
        System.out.println("学习Python 课程");
    }
}
