package com.hcj.springcloud.patternDemo.creational.factoryMethod;

/**
 * date:2019/7/5
 */
public class JavaVideo extends Video {
    @Override
    protected void product() {
        System.out.println("product java video");
    }
}
