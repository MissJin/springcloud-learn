package com.hcj.springcloud.patternDemo.structural.proxy.dynamicProxy;


import com.hcj.springcloud.patternDemo.structural.proxy.IOrderService;
import com.hcj.springcloud.patternDemo.structural.proxy.Order;
import com.hcj.springcloud.patternDemo.structural.proxy.OrderServiceImpl;

/**
 * date:2019/7/22
 * 动态代理测试
 */
public class Main {
    public static void main(String[] args) {

        Order order = new Order();
        order.setUserId(4);

        IOrderService dynamicProxy = (IOrderService) new OrderServiceDynamicProxy(new OrderServiceImpl()).bind();
        dynamicProxy.saveOrder(order);
        dynamicProxy.delete(order);
    }
}
