package com.hcj.springcloud.patternDemo.structural.bridge;

/**
 * date:2019/7/16
 */
public abstract class Bank {
    protected Account account;

    public Bank(Account account) {
        this.account = account;
    }

    abstract Account openAccount();

}
