package com.hcj.springcloud.patternDemo.structural.proxy.db;


import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * date:2019/7/22
 */
public class DynamicDataSource extends AbstractRoutingDataSource {

    @Override
    protected Object determineCurrentLookupKey() {
        return DataSourceContextHolder.getDBType();
    }
}
