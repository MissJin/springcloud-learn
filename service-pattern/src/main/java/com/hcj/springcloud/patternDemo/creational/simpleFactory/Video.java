package com.hcj.springcloud.patternDemo.creational.simpleFactory;

/**
 * date:2019/7/5
 * 父级抽象类
 */

public abstract class Video {

    protected abstract void product();
}
