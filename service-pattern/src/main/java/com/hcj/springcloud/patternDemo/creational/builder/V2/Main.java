package com.hcj.springcloud.patternDemo.creational.builder.V2;

/**
 * date:2019/7/11
 */
public class Main {
    public static void main(String[] args) {

        Course build = new Course.CourseBuilder()
                .buildName("java Course")
                .buildArticle("java article")
                .buildPPT("Java PPT")
                .buildVideo("Java video")
                .buildQA("java question and answer")
                .build();
        System.err.println(build);
    }
}
