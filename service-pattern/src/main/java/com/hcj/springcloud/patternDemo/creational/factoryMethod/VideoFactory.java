package com.hcj.springcloud.patternDemo.creational.factoryMethod;

/**
 * date:2019/7/5
 * <p>
 * 工厂抽象类 定义规范
 */
public abstract class VideoFactory {

    abstract Video getVideo();
}
