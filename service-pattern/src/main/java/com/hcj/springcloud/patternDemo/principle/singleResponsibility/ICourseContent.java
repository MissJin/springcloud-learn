package com.hcj.springcloud.patternDemo.principle.singleResponsibility;

/**
 * date:2019/7/5
 * 课程内容
 */
public interface ICourseContent {

    String getCourseName();

    byte[] getCourseVideo();


}
