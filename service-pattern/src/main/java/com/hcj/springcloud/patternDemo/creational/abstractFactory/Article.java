package com.hcj.springcloud.patternDemo.creational.abstractFactory;

/**
 *
 */
public abstract class Article {

    abstract void product();
}
