package com.hcj.springcloud.patternDemo.creational.builder;

import lombok.Data;

/**
 * date:2019/7/11
 */

@Data
public class Course {

    /**
     * name
     */
    private String name;

    /**
     * ppt
     */
    private String PPT;

    /**
     * video
     */
    private String video;

    /**
     * article
     */
    private String article;

    /**
     * question && answer
     */
    private String QA;


}
