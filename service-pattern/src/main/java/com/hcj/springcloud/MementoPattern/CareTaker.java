package com.hcj.springcloud.MementoPattern;

import java.util.ArrayList;
import java.util.List;

/**
 * 监护者
 */
public class CareTaker {
    private List<Memento> mementoList = new ArrayList<>();

    public void add(Memento memento) {
        mementoList.add(memento);
    }

    public Memento get(int index) {
        return mementoList.get(index);
    }
}
