package com.hcj.springcloud.MementoPattern;

/**
 * 备忘录模式：
 * <br>
 * Memento模式用于将对象的状态恢复到先前的状态。纪念品模式属于行为模式类别。
 * <br>---------------概览-----------------<br>
 * Memento模式使用三个actor类。Memento包含要恢复的对象的状态。
 * Originator在Memento对象中创建和存储状态，Caretaker对象负责从Memento恢复对象状态。
 * 我们创建了Memento，Originator和CareTaker课程。
 * MementoPatternDemo，我们的演示课，将使用看守和发起人的对象，显示对象状态的恢复。
 */
public class MementoPatternDemo {
    public static void main(String[] args) {
        Originator originator = new Originator();
        CareTaker careTaker = new CareTaker();
        originator.setState("state #1");
        originator.setState("state #2");
        careTaker.add(originator.saveStateToMemento());

        originator.setState("state #3");
        careTaker.add(originator.saveStateToMemento());

        originator.setState("state #4");
        System.out.println("current state is : "+ originator.getState());

        originator.getStateFromMemento(careTaker.get(0));
        System.out.println("first saved state:"+ originator.getState());

        originator.getStateFromMemento(careTaker.get(1));
        System.out.println("second saved state:"+ originator.getState());


    }
}
