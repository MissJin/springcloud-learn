package com.hcj.springcloud.StrategyPattern;

/**
 * 接口：策略
 */
public interface Strategy {
    int doOperation(int num1, int num2);
}
