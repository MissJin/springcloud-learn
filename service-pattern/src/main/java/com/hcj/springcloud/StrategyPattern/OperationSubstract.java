package com.hcj.springcloud.StrategyPattern;

/**
 * 减法策略
 */
public class OperationSubstract implements Strategy {
    @Override
    public int doOperation(int num1, int num2) {
        return num1 - num2;
    }
}
