package com.hcj.springcloud.StrategyPattern;
/**
 * 策略模式：
 * <br>
 * 在策略模式中，可以在运行时更改类行为或其算法。
 * 这种类型的设计模式属于行为模式。
 * 在策略模式中，我们创建表示各种策略的对象和根据其策略对象行为不同的上下文对象。
 * 策略对象更改上下文对象的执行算法
 * <br>---------------概览-----------------<br>
 * 我们将创建一个策略接口，用于定义实现Strategy接口的操作和具体策略类。
 * Context是一个使用Strategy的类。
 * StrategyPatternDemo是我们的演示类，
 * 它将使用Context和strategy对象来演示基于它部署或使用的策略的Context行为的变化。
 */
public class StrategyPatternDemo {
    public static void main(String[] args) {
        Context context = new Context(new OperationAdd());
        System.out.println("10+5="+context.executeStrategy(10,5));

        context = new Context(new OperationSubstract());
        System.out.println("10-5="+context.executeStrategy(10,5));

        context = new Context(new OperationMultiply());
        System.out.println("10*5="+context.executeStrategy(10,5));

        context = new Context(new OperationDivide());
        System.out.println("10/5="+context.executeStrategy(10,5));

        //        10+5=15
        //        10-5=5
        //        10*5=50
        //        10/5=2
    }
}
