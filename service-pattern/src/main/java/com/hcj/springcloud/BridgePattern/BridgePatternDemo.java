package com.hcj.springcloud.BridgePattern;

/**
 * 当我们需要将抽象与其实现分离时，使用Bridge，以便两者可以独立变化。
 * 这种类型的设计模式属于结构模式，因为这种模式通过在它们之间提供桥接结构来分离实现类和抽象类。
 * 这种模式涉及一个接口，它充当一个桥梁，使具体类的功能独立于接口实现者类。两种类型的类都可以在结构上进行更改而不会相互影响。
 */
public class BridgePatternDemo {
    public static void main(String[] args) {
        Shape redCircle = new Circle(1,1,1, new RedCircle());
        Shape greenCircle = new Circle(2,1,1, new GreenCircle());
        redCircle.draw();
        greenCircle.draw();
    }

}
