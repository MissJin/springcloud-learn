package com.hcj.springcloud.BridgePattern;

/**
 * 绿色画笔画圆
 */
public class GreenCircle implements DrawAPI {
    @Override
    public void drawCircle(int radius, int x, int y) {
        System.out.printf("绿色画笔画圆-GreenCircle::drawCircle radius=%s x=%s y=%s \n", radius, x, y);
    }
}
