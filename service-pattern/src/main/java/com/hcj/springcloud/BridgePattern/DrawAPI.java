package com.hcj.springcloud.BridgePattern;

/**
 * 对外暴露的画画api
 */
public interface DrawAPI {
    /**
     * 画圆
     * @param radius
     * @param x
     * @param y
     */
    void drawCircle(int radius, int x, int y);
}
