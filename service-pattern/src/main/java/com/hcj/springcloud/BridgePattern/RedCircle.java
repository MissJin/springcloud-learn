package com.hcj.springcloud.BridgePattern;

/**
 * 红色画笔画圆
 */
public class RedCircle implements DrawAPI {
    @Override
    public void drawCircle(int radius, int x, int y) {
        System.out.printf("红色画笔画圆-RedCircle::drawCircle radius=%s x=%s y=%s \n", radius, x, y);
    }
}
