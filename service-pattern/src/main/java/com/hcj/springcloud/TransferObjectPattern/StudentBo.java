package com.hcj.springcloud.TransferObjectPattern;

import java.util.ArrayList;
import java.util.List;

public class StudentBo {
    private List<StudentVo> studentVoList;

    public StudentBo() {
        studentVoList = new ArrayList<>();
        studentVoList.add(new StudentVo("student0",0));
        studentVoList.add(new StudentVo("student1",1));
    }

    public List<StudentVo> getAllStudents(){
        return studentVoList;
    }

    public void updateStudent(StudentVo studentVo){
        studentVoList.get(studentVo.getRollNo()).setName(studentVo.getName());
        System.out.println("student：编号为："+studentVo.getRollNo()+ " , updated in the database");
    }

    public StudentVo getStudent(int rollNo){
        return studentVoList.get(rollNo);
    }

    public void deleteStudent(StudentVo studentVo){
        studentVoList.remove(studentVo);
        System.out.println("student：编号为："+studentVo.getRollNo()+ " , deleted from database");
    }

}
