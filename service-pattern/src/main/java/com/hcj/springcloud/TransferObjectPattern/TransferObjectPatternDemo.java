package com.hcj.springcloud.TransferObjectPattern;

import java.util.List;

/**
 * 传输对象模式：
 * <br>
 * 当我们想要从客户端到服务器一次性传递具有多个属性的数据时，使用传输对象模式。
 * 传输对象也称为值对象。
 * Transfer Object是一个简单的POJO类，具有getter / setter方法，并且是可序列化的，因此可以通过网络传输。
 * 它没有任何行为。
 * 服务器端业务类通常从数据库中提取数据并填充POJO并将其发送到客户端或按值传递。
 * 对于客户端，传输对象是只读的。
 * 客户端可以创建自己的传输对象并将其传递给服务器，以便一次性更新数据库中的值。
 * 以下是此类设计模式的实体。
 * <pre>
 * 业务对象 - 业务服务使用数据填充传输对象。
 * 传输对象 - 简单的POJO，具有仅设置/获取属性的方法。
 * 客户端 - 客户端请求或将传输对象发送到业务对象。
 * </pre>
 * <br>---------------概览-----------------<br>
 * 我们将创建一个StudentBO作为业务对象，学生作为转移对象来表示我们的实体。
 * TransferObjectPatternDemo，我们的演示类，在这里充当客户端，
 * 将使用StudentBO和 Student来演示传输对象设计模式。
 */
public class TransferObjectPatternDemo {
    public static void main(String[] args) {
        StudentBo studentBo = new StudentBo();
        List<StudentVo> allStudents = studentBo.getAllStudents();
        System.err.println("所有学生---------");
        allStudents.stream().forEach(x-> System.out.println(x));

        StudentVo studentVo = allStudents.get(0);
        studentVo.setName("hcj");
        studentBo.updateStudent(studentVo);

        StudentVo studentVo1 = studentBo.getStudent(0);
        System.out.println(studentVo1);

        //        所有学生---------
        //        StudentVo{name='student0', rollNo=0}
        //        StudentVo{name='student1', rollNo=1}
        //        student：编号为：0 , updated in the database
        //        StudentVo{name='hcj', rollNo=0}
    }
}
