package com.hcj.springcloud.VisitorPattern;
/**
 * 访问者模式：
 * <br>
 * 在访问者模式中，我们使用访问者类来更改元素类的执行算法。
 * 通过这种方式，元素的执行算法可以随着访问者的变化而变化。
 * 此模式属于行为模式类别。
 * 根据模式，元素对象必须接受访问者对象，以便访问者对象处理元素对象上的操作。
 * <br>---------------概览-----------------<br>
 * 我们将创建一个定义接受操作的ComputerPart接口。
 * 键盘，鼠标，监视器和计算机是实现ComputerPart接口的具体类。
 * 我们将定义另一个接口ComputerPartVisitor，它将定义一个访问者类操作。
 * 计算机使用具体访问者做相应的动作。
 * VisitorPatternDemo，我们的演示类，将使用Computer和ComputerPartVisitor类来演示访问者模式的使用。
 */
public class VisitorPatternDemo {
    public static void main(String[] args) {
        Computer computer = new Computer();
        computer.accept(new ComputerPartDisplayVisitor());
        //        ComputerPartDisplayVisitor:visit() monitor
        //        ComputerPartDisplayVisitor:visit() mouse
        //        ComputerPartDisplayVisitor:visit() keyBoard
        //        ComputerPartDisplayVisitor:visit() computer
    }
}
