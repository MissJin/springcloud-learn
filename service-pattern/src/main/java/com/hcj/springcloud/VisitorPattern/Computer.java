package com.hcj.springcloud.VisitorPattern;

import java.util.ArrayList;
import java.util.List;

public class Computer implements ComputerPart {
    @Override
    public void accept(ComputerPartVisitor computerPartVisitor) {
        for (int i = 0; i < parts.size(); i++) {
            parts.get(i).accept(computerPartVisitor);
        }
        computerPartVisitor.visit(this);
    }

    private List<ComputerPart> parts = new ArrayList<>();

    public Computer() {
        parts.add(new Monitor());
        parts.add(new Mouse());
        parts.add(new KeyBoard());
    }


}
