package com.hcj.springcloud.VisitorPattern;

public class ComputerPartDisplayVisitor implements ComputerPartVisitor {
    @Override
    public void visit(Computer computer) {
        System.out.println("ComputerPartDisplayVisitor:visit() computer");
    }

    @Override
    public void visit(Mouse mouse) {
        System.out.println("ComputerPartDisplayVisitor:visit() mouse");
    }

    @Override
    public void visit(KeyBoard keyBoard) {
        System.out.println("ComputerPartDisplayVisitor:visit() keyBoard");
    }

    @Override
    public void visit(Monitor monitor) {
        System.out.println("ComputerPartDisplayVisitor:visit() monitor");
    }
}
