package com.hcj.springcloud.VisitorPattern;


public interface ComputerPartVisitor {
    void visit(Computer computer);
    void visit(Mouse mouse);
    void visit(KeyBoard keyBoard);
    void visit(Monitor monitor);
}
