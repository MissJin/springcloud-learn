package com.hcj.springcloud.FrontControllerPattern;

/**
 * 前端控制器设计模式：
 * <br>
 * 前端控制器设计模式用于提供集中的请求处理机制，以便所有请求将由单个处理程序处理。
 * 此处理程序可以执行身份验证/授权/日志记录或跟踪请求，然后将请求传递给相应的处理程序。
 * 以下是此类设计模式的实体。
 * 前端控制器 - 用于处理应用程序的各种请求的单一处理程序（基于Web或基于桌面）。
 * Dispatcher - Front Controller可以使用调度程序对象，该对象可以将请求分派给相应的特定处理程序。
 * 视图 - 视图是发出请求的对象。
 * <br>---------------概览-----------------<br>
 * 我们将创建一个FrontController和Dispatcher来相应地充当Front Controller和Dispatcher。
 * HomeView和StudentView表示可以向前端控制器发出请求的各种视图。
 * FrontControllerPatternDemo，我们的演示类，将使用FrontController演示Front Controller Design Pattern。
 */
public class FrontControllerPatternDemo {
    public static void main(String[] args) {
        FrontController frontController = new FrontController();
        frontController.dispatchRequest("home");
        //        page requested:home
        //        user is authenticated successfully!
        //        Dispatcher::dispatch()
        //        HomeView::show()
        frontController.dispatchRequest("student");
        //        page requested:student
        //        user is authenticated successfully!
        //        Dispatcher::dispatch()
        //        StudentView::show()
    }
}
