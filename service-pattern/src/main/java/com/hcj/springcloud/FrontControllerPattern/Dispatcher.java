package com.hcj.springcloud.FrontControllerPattern;

public class Dispatcher {
    private StudentView studentView;
    private HomeView homeView;

    public Dispatcher(){
        studentView = new StudentView();
        homeView = new HomeView();
    }

    public void dispatch(String request){
        System.out.println("Dispatcher::dispatch()");
        if (request.equalsIgnoreCase("student")){
            studentView.show();
        }else {
            homeView.show();
        }
    }

}
