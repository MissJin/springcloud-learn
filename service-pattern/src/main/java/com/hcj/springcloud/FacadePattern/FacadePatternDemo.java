package com.hcj.springcloud.FacadePattern;

/**
 * 外观模式：
 *  Facade模式隐藏了系统的复杂性，并为客户端提供了一个客户端可以访问系统的接口。
 *  这种类型的设计模式属于结构模式，因为此模式为现有系统添加了一个接口以隐藏其复杂性。
 *
 * 此模式涉及单个类，它提供客户端所需的简化方法，并委托对现有系统类的方法的调用。
 */
public class FacadePatternDemo {
    public static void main(String[] args) {
        ShapeMaker shapeMaker = new ShapeMaker();
        shapeMaker.drawCircle();
        shapeMaker.drawRectangle();
        shapeMaker.drawSquare();
    }
}
