package com.hcj.springcloud.FacadePattern;

public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println("Circle::draw() method.");
    }
}
