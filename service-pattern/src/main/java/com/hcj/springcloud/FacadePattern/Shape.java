package com.hcj.springcloud.FacadePattern;

public interface Shape {
    void draw();
}
