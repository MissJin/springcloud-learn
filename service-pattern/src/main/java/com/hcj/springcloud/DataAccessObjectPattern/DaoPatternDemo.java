package com.hcj.springcloud.DataAccessObjectPattern;
/**
 * 数据访问对象模式/DAO模式：
 * <br>
 * 数据访问对象模式或DAO模式用于将低级数据访问API或操作与高级业务服务分开。
 * 以下是数据访问对象模式的参与者。
 * 数据访问对象接口 - 此接口定义要对模型对象执行的标准操作。
 * Data Access Object具体类 - 该类实现上述接口。
 * 此类负责从数据源获取数据，该数据源可以是database / xml或任何其他存储机制。
 * 模型对象或值对象 - 此对象是包含get / set方法的简单POJO，用于存储使用DAO类检索的数据
 * <br>---------------概览-----------------<br>
 * 我们将创建一个充当模型或值对象的Student对象。
 * StudentDao是数据访问对象接口。StudentDaoImpl是实现数据访问对象接口的具体类。
 * 我们的演示类DaoPatternDemo将使用StudentDao来演示数据访问对象模式的使用。
 */
public class DaoPatternDemo {
    public static void main(String[] args) {
        StudentDao studentDao = new StudentDaoImpl();
        // print all students
        for (Student student: studentDao.getAllStudents()){
            System.out.println(student);
        }

        // update
        Student student = studentDao.getAllStudents().get(0);
        student.setName("hcj1");
        studentDao.updateStudent(student);

        // get
        Student student1 = studentDao.getStudent(0);
        System.out.println("get student:"+ student1);
        //        Student{name='hcj', rollNo=0}
        //        Student{name='lff', rollNo=1}
        //        update student:Student{name='hcj1', rollNo=0}
        //        get student:Student{name='hcj1', rollNo=0}

    }
}
