package com.hcj.springcloud.DataAccessObjectPattern;

import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {
    private List<Student> students = new ArrayList<>();

    public StudentDaoImpl() {
        Student student0 = new Student("hcj",0);
        Student student1 = new Student("lff",1);
        students.add(student0);
        students.add(student1);
    }

    @Override
    public List<Student> getAllStudents() {
        return students;
    }

    @Override
    public void updateStudent(Student student) {
        Student student_ = students.get(student.getRollNo());
        student_.setName(student.getName());
        System.out.println("update student:"+ student_);
    }

    @Override
    public void deleteStudent(Student student) {
        students.remove(student);
        System.out.println("delete student:"+ student);
    }

    @Override
    public void addStudent(Student student) {
        students.add(student);
        System.out.println("add student:"+ student);
    }

    @Override
    public Student getStudent(int rollNo) {
        return students.get(rollNo);
    }
}
