package com.hcj.springcloud.DataAccessObjectPattern;


import java.util.List;

public interface StudentDao {
    List<Student> getAllStudents();
    void updateStudent(Student student);
    void deleteStudent(Student student);
    void addStudent(Student student);
    Student getStudent(int rollNo);

}
