package com.hcj.springcloud.CompositePattern;

import java.math.BigDecimal;

/**
 * 组合模式
 */
public class CompositePatternDemo {
    public static void main(String[] args) {
        Employee ceo = new Employee("黄总","ceo", new BigDecimal(30000));
        Employee empSale = new Employee("售货员-小蓝","sale", new BigDecimal(3000));
        Employee empMarket = new Employee("市场推销-小李","market", new BigDecimal(30000));
        ceo.add(empSale);
        ceo.add(empMarket);
        System.out.println(ceo);
    }
}
