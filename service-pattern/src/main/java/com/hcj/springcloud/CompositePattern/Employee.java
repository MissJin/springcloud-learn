package com.hcj.springcloud.CompositePattern;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Employee {
    private String name;
    private String dept;
    private BigDecimal salary;
    private List<Employee> subEmployee = new ArrayList<>();

    public Employee(String name, String dept, BigDecimal salary) {
        this.name = name;
        this.dept = dept;
        this.salary = salary;
    }

    public void add(Employee employee){
        subEmployee.add(employee);
    }

    public void remove(Employee employee){
        subEmployee.remove(employee);
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public List<Employee> getSubEmployee() {
        return subEmployee;
    }

    public void setSubEmployee(List<Employee> subEmployee) {
        this.subEmployee = subEmployee;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", dept='" + dept + '\'' +
                ", salary=" + salary +
                ", subEmployee=" + subEmployee +
                '}';
    }
}
