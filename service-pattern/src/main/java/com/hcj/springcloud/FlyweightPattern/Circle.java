package com.hcj.springcloud.FlyweightPattern;

public class Circle implements Shape {
    private String color;
    private int radius;
    private int x;
    private int y;

    public Circle(String color) {
        this.color = color;
    }

    public Circle(String color, int radius, int x, int y) {
        this.color = color;
        this.radius = radius;
        this.x = x;
        this.y = y;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void draw() {
        System.out.println("Circle:: draw() method.");
        System.out.println(this);
    }

    @Override
    public String toString() {
        return "Circle{" +
                "color='" + color + '\'' +
                ", radius=" + radius +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
