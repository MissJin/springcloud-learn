package com.hcj.springcloud.FlyweightPattern;

public interface Shape {
    void draw();
}
