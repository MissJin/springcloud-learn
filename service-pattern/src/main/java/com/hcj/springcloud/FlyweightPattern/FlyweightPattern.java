package com.hcj.springcloud.FlyweightPattern;

/**
 * 享元模式：减少对象的重复创建，以提升性能
 * Flyweight模式主要用于减少创建的对象数量，减少内存占用并提高性能。
 * 这种类型的设计模式属于结构模式，因为该模式提供了减少对象数量的方法，从而改善了应用程序的对象结构。
 *
 * Flyweight模式尝试通过存储它们来重用已存在的类似对象，并在找不到匹配对象时创建新对象。我们将通过绘制20个不同位置的圆圈来演示此模式，但我们将仅创建5个对象。
 * 只有5种颜色可用，因此颜色属性用于检查现有的Circle对象。
 */
public class FlyweightPattern {
    private static final String[] colors = {"Red","Green","Blue","White","Black"};
    public static void main(String[] args) {
        for (int i = 0; i< 20; i++){
            Circle circle = (Circle) ShapeFactory.getCircle(getRandomColor());
            circle.setRadius(getRandomRadius(100));
            circle.setX(getRandom(10));
            circle.setY(getRandom(10));
            circle.draw();
            System.out.println();
        }

    }

    private static int getRandomRadius(int max) {
        return (int) (Math.random() * max);
    }

    private static int getRandom(int max) {
        return (int) (Math.random() * max);
    }

    private static String getRandomColor() {
        return colors[(int) (Math.random() * colors.length)];
    }
}
