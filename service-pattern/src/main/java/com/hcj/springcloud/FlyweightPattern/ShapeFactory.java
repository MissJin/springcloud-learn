package com.hcj.springcloud.FlyweightPattern;

import java.util.HashMap;

/**
 * 形状工厂
 */
public class ShapeFactory {
    private static final HashMap circleMap = new HashMap();


    public  static  Shape getCircle(String color){
        Circle circle = (Circle) circleMap.get(color);
        if (circle == null){
            circle = new Circle(color);
            circleMap.put(color, circle);
            System.out.println("creating circle of color: "+ color);
        }
        return circle;
    }
}
